# dagr

`dagr` makes it easy to log your time worked in jira tickets by simply telling it when you start
working on something and when you stop for the day. Times, tickets, and descriptions are provided
through an easy to use command line with thorough bash completion support. Reports are
customizable with jinja2 templating.

Read the [documentation](https://francisferrell.gitlab.io/dagr/) for more information.

