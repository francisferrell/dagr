====
dagr
====

-----------------------------------------------
simple CLI tool for logging time worked to Jira
-----------------------------------------------

:Author: Francis Ferrell <francisferrell@gamil.com>
:Version: 1.0.0
:Manual section: 5

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l

DESCRIPTION
===========

Dagr stores data and configuration in two files within your home directory.

CONFIG
======

Configuration is stored in the default format of python's ``configparser.ConfigParser``. Which
is basicallly ``ini`` format with values that are whitespace trimmed and unquoted.

Example::

    [jira]
    host = https://www.thejirainstance.com
    user = francis
    password = super secret password

Please ``chmod 600`` your configuration file.

DATA
====

The data is stored in an sqlite database file.


FILES
=====

$XDG_CONFIG_HOME/dagr/config.ini
    config file, if $XDG_CONFIG_HOME is defined in the environment

~/.config/dagr/config.ini
    config file, default location

$XDG_DATA_HOME/dagr/data.sqlite3
    database file, if $XDG_DATA_HOME is defined in the environment

~/.local/share/dagr/data.sqlite3
    database file, default location

