====
dagr
====

-----------------------------------------------
simple CLI tool for logging time worked to Jira
-----------------------------------------------

:Author: Francis Ferrell <francisferrell@gamil.com>
:Version: 1.0.0
:Manual section: 1

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l

SYNOPSIS
========

``dagr`` [DATEREF] start [TIMEREF] [TICKET] DESCRIPTION [...]

``dagr`` [DATEREF] end [TIMEREF]

``dagr`` [DATEREF] delete TIMEREF

``dagr`` [DATEREF] report

``dagr`` [DATEREF] upload [-f|--force]

DESCRIPTION
===========

Log time to Jira by telling **dagr** when you start a new task and when you end your work. Upload
everything logged on a given day with a single command.

START
=====

    ``dagr`` [DATEREF] ``start`` [TIMEREF] [TICKET] DESCRIPTION [...]

Start new work. If another entry exists starting at TIMEREF on DATEREF, it will be replaced with
this one.

If Jira integration is configured and DESCRIPTION is omitted, it will be filled in with the summary
of the specified TICKET.

DATEREF

    (optional, defaults to today) the date of the dagr to add the entry to, see **DATEREF**, below

TIMEREF

    (optional, defaults to now) the time that the work started, see **TIMEREF**, below

TICKET

    (optional) the ticket number

DESCRIPTION

    the description of work; optional if TICKET is provided

END
===

    ``dagr`` [DATEREF] ``end`` [TIMEREF]

End work. If another entry exists starting at TIMEREF on DATEREF, it will be deleted.

DATEREF

    (optional, defaults to today) the date of the dagr to end

TIMEREF

    (optional, defaults to now) the time that the work ended

DELETE
======

    ``dagr`` [DATEREF] ``delete`` TIMEREF

Remove an entry from the dagr, or reopen a dagr closed by ``dagr end``.

DATEREF

    (optional, defaults to today) the date of the dagr to modify

TIMEREF

    If corresponding to the start time of work, removes that ticket/description from the dagr.
    If corresponding to the end of a dagr, reopens that dagr.

REPORT
======

    ``dagr`` [DATEREF] ``report``

Report the contents of the dagr.

DATEREF

    (optional, defaults to today) the date of the dagr to report

UPLOAD
======

    ``dagr`` [DATEREF] ``upload`` [-f|--force]

End work now and upload the completed dagr to Jira.

DATEREF

    (optional, defaults to today) the date of the dagr to upload

-f, --force

    (optional) upload the dagr, even if it was previously marked as successfully uploaded

All completed but not yet uploaded sequences within the referenced dagr will be uploaded. All
completed and uploaded sequences will be ignored. This means you can safely ``dagr upload`` multiple
times per DATEREF.

However, there is a caveat. ``dagr start``, ``dagr delete``, etc. will continue to work on a completed
and uploaded sequence, even if it creates an inconsistency with the worklog in Jira. Including the
``--force`` flag will cause ``upload`` to upload the latest state of the dagr without regard for
what might already be in Jira. This will create duplicate entries in Jira.

This command performs a ``dagr end`` before uploading. If you need to ``end`` with a particular
TIMEREF, use ``dagr end`` before calling this command. Be careful when combining with DATEREF as
specifying a DATEREF without a TIMEREF can be surprising: it uses the current time on the date
specified.

EXAMPLES
========

* start your day

    **dagr start email and coffee**

* oh wait, you've been working on something for 10 minutes now

    **dagr start -10m fix dagr bugs**

* start work on a ticket, taking the description from Jira

    **dagr start wkl-17**

* log something at a specific time

    **dagr start @9:30 v2 design meeting**

* meeting got derailed, so change its description

    **dagr start @9:30 put out production fires**

* delete something

    **dagr delete @14:30**

* it's time to go home

    **dagr end**

    **dagr upload**

* what did you work on yesterday?

    **dagr @yesterday report**

* forgot to upload to Jira before leaving yesterday

    **dagr @yesterday upload**

DATEREF
=======

All commands accept an optional **DATEREF**. If omitted, the default is today.

**DATEREF** accepts a variety of formats to reference dates absolutely or relatively. Supported
formats include::

    @YYYY-MM-DD

    @keyword

    [-|+]Xd

The first form is an absolute reference to a specific year, month, and day. Month and day values
must be zero-padded.

The second form is a name of a day that is interpreted relative to today. Possible values for
**keyword** are: today, yesterday, monday, mon, tuesday, tues, tue, wednesday, wed, thursday,
thurs, thur, thu, friday, fri, saturday, sat, sunday, and sun. Weekdays are interpreted to be in
the same week as today with the week beginning on Monday. Put another way: other than whey they are
today, **@sun** is always in the future and **@mon** is always in the past.

The third form is an offset of **X** days from today, e.g. **-1d** is equivalent to **@yesterday**,
**+1d** is tomorrow, and **-7d** is one week ago.

TIMEREF
=======

Some commands accept a **TIMEREF**. If omitted, the default is now.

**TIMEREF** accepts two formats, absolute and relative::

    @HH:MM

    [-|+]XhYm

The first form is an absolute reference to a specific hour and minute. Hour must be in 24-hour
notation. Hour may be zero-padded, but it is not required. Minute must be zero-padded.

The second form is an offset of **X** hours and **Y** minutes from now. Either component may be
omitted if it is **0**.

BASH COMPLETION
===============

Tab completion is provided for bash. Shortcuts for all @keyword **DATEREF** values, each command
name, and some options are provided. **TIMEREF** values from today's dagr are completed for
commands that take it. **TICKET** values from today's and yesterday's dagr are completed for
**dagr start**.

