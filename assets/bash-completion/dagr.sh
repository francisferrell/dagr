
_dagr(){
    # rewrite COMP_WORDS and COMP_CWORD locally *without* : being a word break character
    local -a comp_words
    local comp_cword
    __reassemble_comp_words_by_ref : comp_words comp_cword

    local DAGR_SQL="${XDG_DATA_HOME:-${HOME}/.local/share}/dagr/data.sqlite3"
    local -r COMMANDS='start end delete report upload'

    # format start field as @HH:MM, stripping zero-padding on the hour because
    # sqlite's strftime doesn't have a H formatter, only HH
    local -r start_as_at_hh_mm="replace('@'||strftime('%H:%M', start, 'unixepoch', 'localtime'), '@0', '@')"

    # sqlite's weird datetime keyword modification system, resolves to midnight at the beginning of today
    local -r start_of_day="'now', 'localtime', 'start of day', 'utc'"
    # and midnight at the end of today
    local -r end_of_day="'now', 'localtime', '+1 day', 'start of day', 'utc'"

    _sql() {
        if [[ -e $DAGR_SQL ]] ; then
            sqlite3 "$DAGR_SQL" -noheader "$@"
        fi
    }

    _sql_start_times_from_today() {
        _sql "select ${start_as_at_hh_mm} from tasks where datetime(start, 'unixepoch') BETWEEN datetime(${start_of_day}) AND datetime(${end_of_day});"
    }

    # dagr [DATEREF] CMD [...]
    _complete() {
        if [[ -n $dateref ]] ; then
            echo "$COMMANDS"
        elif [[ $this_word =~ ^@ ]] ; then
            echo '@mon @tue @wed @thu @fri @sat @sun @yesterday'
        else
            echo "${COMMANDS} @"
        fi
    }

    # dagr [DATEREF] start [TIMEREF] [TICKET] DESCRIPTION [...]
    _complete_start() {
        local timeref=''
        if [[ $1 =~ ^[-+@] ]] ; then
            timeref="$1"
            shift
        fi

        if [[ -z $timeref ]] ; then
            _sql_start_times_from_today
        fi
        if [[ -z $1 ]] ; then
            _sql "select distinct ticket from tasks where klass = 'Task' and datetime(start, 'unixepoch') >= datetime(${start_of_day},'-1 day');"
        fi
    }

    # dagr [DATEREF] end [TIMEREF]
    #_complete_end() {}

    # dagr [DATEREF] delete TIMEREF
    _complete_delete() {
        if [[ -z $1 ]] ; then
            _sql_start_times_from_today
        fi
    }

    # dagr [DATEREF] report
    #_complete_report() {}

    # dagr [DATEREF] upload [-f|--force]
    _complete_upload() {
        echo '-f --force'
    }

    # from the args in comp_words up to but NOT INCLUDING the word under the cursor:
    # 1. silently ignore -h and --help
    # 2. grab dateref, if present
    # 3. grab cmd, if present
    # 4. put all other args (again, *except* the one under the cursor) in remainder
    local -r this_word="${comp_words[$comp_cword]}"
    local dateref=''
    local cmd=''
    local -i i
    local tw
    for (( i = 1; i < comp_cword; i++ )) ; do
        if [[ -n $cmd ]] ; then
            break
        fi
        tw="${comp_words[$i]}"

        if [[ $tw == -h || $tw == --help ]] ; then
            continue
        elif [[ -z $dateref && $tw =~ ^[-+@] ]] ; then
            dateref="$tw"
        else
            cmd="$tw"
        fi
    done

    local -i d
    let d='comp_cword - i'
    if [[ $d -lt 0 ]] ; then
        d=0
    fi
    local -r -a remainder=( ${comp_words[@]:$i:$d} )

    # find our completion function based on cmd, bail if a function for cmd isn't defined
    local _complete
    if [[ -n $cmd ]] ; then
        _complete="_complete_${cmd}"
        if [[ $( type -t "$_complete" ) != 'function' ]] ; then
            return
        fi
    else
        _complete="_complete"
    fi

    # pass remainder to the completion function and let it define the choices for compgen
    local choices="$( "$_complete" "${remainder[@]}" )"
    COMPREPLY=( $( compgen -W "$choices" -- "$this_word" ) )

    __ltrim_colon_completions "$this_word"
}

complete -F _dagr dagr

