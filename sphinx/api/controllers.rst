
Controllers
===========

.. toctree::

    controllers.base
    controllers.main
    controllers.start
    controllers.end
    controllers.delete
    controllers.report
    controllers.upload

.. automodule:: dagr.controllers

