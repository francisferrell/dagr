
API
###

.. toctree::
    :hidden:

    models
    controllers
    helpers
    libdagr

.. automodule:: dagr

