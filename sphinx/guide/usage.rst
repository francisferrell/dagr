
Usage
=====

.. code-block:: bash

    dagr [DATEREF] start [TIMEREF] [TICKET] DESCRIPTION [...]
    dagr [DATEREF] end [TIMEREF]
    dagr [DATEREF] delete TIMEREF
    dagr [DATEREF] report
    dagr [DATEREF] upload [-f|--force]


Bash Completion
---------------

Tab completion is provided for bash. Shortcuts for all @keyword ``DATEREF`` values, each command
name, and some options are provided. ``TIMEREF`` values from today's dagr are completed for
commands that take it. ``TICKET`` values from today's and yesterday's dagr are completed for
``dagr start``.

Bash 4 and your distribution's ``bash-completion`` package are required for completion to work
properly.


dagr start
----------

.. code-block:: bash

    dagr [DATEREF] start [TIMEREF] [TICKET] DESCRIPTION [...]

Start new work. If another entry exists starting at TIMEREF on DATEREF, it will be replaced with
this one.

If Jira integration is configured and DESCRIPTION is omitted, it will be filled in with the summary
of the specified TICKET.

:DATEREF:
    (optional, defaults to today) the date of the dagr to add the entry to, see :doc:`dateref`.

:TIMEREF:
    (optional, defaults to now) the time that the work started, see :doc:`timeref`.

:TICKET:
    (optional) the ticket number

:DESCRIPTION:
    the description of work; optional if ``TICKET`` is provided


dagr end
--------

.. code-block:: bash

    dagr [DATEREF] end [TIMEREF]

End work. If another entry exists starting at TIMEREF on DATEREF, it will be deleted.

:DATEREF:
    (optional, defaults to today) the date of the dagr to end, see :doc:`dateref`.

:TIMEREF:
    (optional, defaults to now) the time that the work ended, see :doc:`timeref`.


dagr delete
-----------

.. code-block:: bash

    dagr [DATEREF] delete TIMEREF

Remove an entry from the dagr, or reopen a dagr closed by ``dagr end``.

:DATEREF:
    (optional, defaults to today) the date of the dagr to modify, see :doc:`dateref`.

:TIMEREF:
    If corresponding to the start time of work, removes that ticket/description from the dagr.
    If corresponding to the end of a dagr, reopens that dagr.

    see :doc:`timeref`.


dagr report
-----------

.. code-block:: bash

    dagr [DATEREF] report

Report the contents of the dagr.

:DATEREF:
    (optional, defaults to today) the date of the dagr to report, see :doc:`dateref`.


dagr upload
-----------

.. code-block:: bash

    dagr [DATEREF] upload [-f|--force]

End work now and upload the completed dagr to Jira.

:DATEREF:
    (optional, defaults to today) the date of the dagr to upload, see :doc:`dateref`.

:-f or \\-\\-force:
    (optional) upload the dagr, even it it was previously marked as successfully uploaded

All completed but not yet uploaded sequences within the referenced dagr will be uploaded. All
completed and uploaded sequences will be ignored. This means you can safely ``dagr upload`` multiple
times per ``DATEREF``.

However, there is a caveat. ``dagr start``, ``dagr delete``, etc. will continue to work on a completed
and uploaded sequence, even if it creates an inconsistency with the dagr as it was uploaded to
Jira. Including the ``--force`` flag will cause ``upload`` to upload the latest state of the dagr
without regard for what might already be in Jira. This will create duplicate entries in Jira.

This command performs a ``dagr end`` before uploading. If you need to ``end`` with a particular
TIMEREF, use ``dagr end`` before calling this command. Be careful when combining with DATEREF as
specifying a DATEREF without a TIMEREF can be surprising: it uses the current time on the date
specified.

