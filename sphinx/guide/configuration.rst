
Configuration
=============

config.ini
----------

Configuration is stored in the default format of python's ``configparser.ConfigParser``. Which
is basicallly ``ini`` format with values that are whitespace trimmed and unquoted.

``dagr`` searches for configuration first in ``$XDG_CONFIG_HOME/dagr/config.ini``, if
``XDG_CONFIG_HOME`` is in the environment, then in ``~/.config/dagr/config.ini``.

.. code-block:: ini

    [jira]
    host = https://www.thejirainstance.com
    user = francis
    password = super secret password

Please ``chmod 600`` your configuration file.

data.sqlite3
------------

The data is stored in an sqlite database file.

``dagr`` searches for the database first in ``$XDG_DATA_HOME/dagr/data.sqlite3``, if
``XDG_DATA_HOME`` is in the environment, then in ``~/.local/share/dagr/data.sqlite3``.

