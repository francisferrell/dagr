
dagr
####

Installation
------------

Source is available on `GitLab <https://gitlab.com/francisferrell/dagr>`_.

Ubuntu
^^^^^^

I package ``dagr`` for Ubuntu bionic and later.

First, follow the instructions for adding my `apt repository <https://francisferrell.dev/>`_. Then,
install dagr:

.. code-block:: bash

    sudo apt update; sudo apt install dagr


Basic Usage
-----------

``dagr`` makes it as easy to log your time worked in jira tickets by simply telling it when you
start working on something and when you stop for the day. Times, tickets, and descriptions are
provided through an easy to use command line with thorough bash completion support. Reports are
customizable with jinja2 templating.

**Example workday**:

.. code-block:: none

    Aug 27 at 8:00 > francis@deathcab
    ~ >> dagr start email and coffee

    Aug 27 at 8:15 > francis@deathcab
    ~ >> dagr start wkl-2

    Aug 27 at 9:37 > francis@deathcab
    ~ >> dagr start wkl-3

    Aug 27 at 11:55 > francis@deathcab
    ~ >> dagr end

    Aug 27 at 12:38 > francis@deathcab
    ~ >> dagr start wkl-4

    Aug 27 at 13:07 > francis@deathcab
    ~ >> dagr start wkl-3

    Aug 27 at 16:43 > francis@deathcab
    ~ >> dagr end

    Aug 27 at 16:43 > francis@deathcab
    ~ >> dagr report
      Mon, 27 Aug 2018

      start    end  duration     ticket  description
       8:00   8:15       15m             email and coffee
       8:15   9:37    1h 22m      wkl-2  implement command line interface
       9:37  11:55    2h 18m      wkl-3  fix bugs
      12:38  13:07       29m      wkl-4  fix more bugs
      13:07  16:43    3h 36m      wkl-3  fix bugs

                      1h 22m      wkl-2
                      5h 54m      wkl-3
                         29m      wkl-4
                      7h 45m      total

    Aug 27 at 16:43 > francis@deathcab
    ~ >> dagr upload
    uploading 1h 22m to wkl-2... OK
    uploading 2h 18m to wkl-3... OK
    uploading 29m to wkl-4... OK
    uploading 3h 36m to wkl-3... OK

