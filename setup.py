
from setuptools import setup, find_packages

import os
from pathlib import Path
import sys

sys.path.insert( 0, os.path.abspath( '.' ) )
import dagr


def glob_match( glob ):
    root = Path( 'dagr' )
    return [ str( p.relative_to( root ) ) for p in root.glob( glob ) ]


setup(
    name = dagr.__title__,
    version = '0.0' if dagr.__version__ == 'git' else dagr.__version__,
    description = dagr.__description__,
    author = dagr.__author__,
    author_email = dagr.__author_email__,
    url = dagr.__url__,
    license = dagr.__license__,

    packages = find_packages(),

    package_data = dict(
        dagr = glob_match( 'views/**/*.j2' ),
    ),

    entry_points = dict(
        console_scripts = [
            'dagr = dagr.controllers.main:main'
        ],
    )
)

