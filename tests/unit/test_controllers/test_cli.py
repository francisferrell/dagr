
from datetime import datetime
import pytest

from dagr.controllers.base import BaseArguments
from dagr.controllers import start, end, delete, upload, report



@pytest.fixture( scope = 'session', params = [ '@now', '-30m' ] )
def valid_timeref( request ):
    return request.param

@pytest.fixture( scope = 'session', params = [ '@0:60', '-99' ] )
def invalid_timeref( request ):
    return request.param

@pytest.fixture( scope = 'session', params = [ '@then', '--option' ] )
def not_a_timeref( request ):
    return request.param



@pytest.fixture( scope = 'session', params = [ '@today', '@mon' ] )
def valid_dateref( request ):
    return request.param

@pytest.fixture( scope = 'session', params = [ '@2017-00-01', '-1' ] )
def invalid_dateref( request ):
    return request.param

@pytest.fixture( scope = 'session', params = [ '@then', '-o' ] )
def not_a_dateref( request ):
    return request.param



@pytest.fixture( scope = 'session', params = [
    [],
    [ 'foo' ],
    [ 'foo', 'bar' ],
] )
def valid_description( request ):
    return request.param, ' '.join( request.param )



@pytest.fixture( scope = 'session', params = [ '-h', '--help' ] )
def help_option( request ):
    return request.param



def test_parse_args_empty():
    parsed = BaseArguments.parse( [ 'dagr' ] )
    assert parsed.empty
    assert not parsed.help



@pytest.mark.parametrize( 'args', (
    [ 'dagr', None ],

    [ 'dagr', None, 'start' ],
    [ 'dagr', 'end', None ],

    [ 'dagr', None, '@today', 'report' ],
    [ 'dagr', '@today', None, 'report' ],
    [ 'dagr', '@today', 'report', None ],

    [ 'dagr', None, 'end', '@now' ],
    [ 'dagr', 'end', None, '@now' ],
    [ 'dagr', 'end', '@now', None ],

    [ 'dagr', None, '@today', 'end', '@now' ],
    [ 'dagr', '@today', None, 'end', '@now' ],
    [ 'dagr', '@today', 'end', None, '@now' ],
    [ 'dagr', '@today', 'end', '@now', None ],

    [ 'dagr', None, '@today', 'start', '@now', 'foo', 'bar', 'baz' ],
    [ 'dagr', '@today', None, 'start', '@now', 'foo', 'bar', 'baz' ],
    [ 'dagr', '@today', 'start', None, '@now', 'foo', 'bar', 'baz' ],
    [ 'dagr', '@today', 'start', '@now', None, 'foo', 'bar', 'baz' ],
    [ 'dagr', '@today', 'start', '@now', 'foo', None, 'bar', 'baz' ],
    [ 'dagr', '@today', 'start', '@now', 'foo', 'bar', None, 'baz' ],
    [ 'dagr', '@today', 'start', '@now', 'foo', 'bar', 'baz', None ],
) )
def test_help_can_be_anywhere( args, help_option ):
    args = [ help_option if arg is None else arg for arg in args ]
    parsed = BaseArguments.parse( args )
    assert parsed.help



def test_help_can_be_repeated( help_option ):
    parsed = BaseArguments.parse(
        [ 'dagr', help_option, '@today', help_option, 'end', help_option, '@now', help_option ]
    )
    assert parsed.help



def test_help_is_not_in_the_description( help_option ):
    parsed = BaseArguments.parse(
        [ 'dagr', 'start', help_option, 'foo', help_option, 'bar', help_option ]
    )
    assert parsed.help
    assert 'foo bar' == parsed.description



def test_parse_start_simple_args( valid_description ):
    args, expected_description = valid_description
    parsed = BaseArguments.parse( [ 'dagr', 'start' ] + args )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'start'
    assert parsed.dateref == '@today'
    assert parsed.timeref == '@now'
    assert parsed.description == expected_description



def test_parse_start_args_with_dateref( valid_description, valid_dateref ):
    args, expected_description = valid_description
    parsed = BaseArguments.parse( [ 'dagr', valid_dateref, 'start' ] + args )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'start'
    assert parsed.dateref == valid_dateref
    assert parsed.timeref == '@now'
    assert parsed.description == expected_description



def test_parse_start_args_with_timeref( valid_description, valid_timeref ):
    args, expected_description = valid_description
    parsed = BaseArguments.parse( [ 'dagr', 'start', valid_timeref ] + args )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'start'
    assert parsed.dateref == '@today'
    assert parsed.timeref == valid_timeref
    assert parsed.description == expected_description



def test_parse_start_args_with_bothref( valid_description, valid_dateref, valid_timeref ):
    args, expected_description = valid_description
    parsed = BaseArguments.parse( [ 'dagr', valid_dateref, 'start', valid_timeref ] + args )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'start'
    assert parsed.dateref == valid_dateref
    assert parsed.timeref == valid_timeref
    assert parsed.description == expected_description



def test_parse_start_args_ticket_or_description():
    parsed = BaseArguments.parse( [ 'dagr', 'start', 'something' ] )
    assert parsed.description == 'something'
    assert parsed.ticket is None

    parsed = BaseArguments.parse( [ 'dagr', 'start', 'tkt-123' ] )
    assert parsed.description == ''
    assert parsed.ticket == 'tkt-123'



def test_parse_end_simple_args():
    parsed = BaseArguments.parse( [ 'dagr', 'end' ] )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'end'
    assert parsed.dateref == '@today'
    assert parsed.timeref == '@now'



def test_parse_end_args_with_dateref( valid_dateref ):
    parsed = BaseArguments.parse( [ 'dagr', valid_dateref, 'end' ] )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'end'
    assert parsed.dateref == valid_dateref
    assert parsed.timeref == '@now'



def test_parse_end_args_with_timeref( valid_timeref ):
    parsed = BaseArguments.parse( [ 'dagr', 'end', valid_timeref ] )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'end'
    assert parsed.dateref == '@today'
    assert parsed.timeref == valid_timeref



def test_parse_end_args_with_bothref( valid_dateref, valid_timeref ):
    parsed = BaseArguments.parse( [ 'dagr', valid_dateref, 'end', valid_timeref ] )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'end'
    assert parsed.dateref == valid_dateref
    assert parsed.timeref == valid_timeref



def test_parse_report_simple_args():
    parsed = BaseArguments.parse( [ 'dagr', 'report' ] )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'report'
    assert parsed.dateref == '@today'



def test_parse_report_args_with_dateref( valid_dateref ):
    parsed = BaseArguments.parse( [ 'dagr', valid_dateref, 'report' ] )
    assert not parsed.empty
    assert not parsed.help
    assert parsed.command == 'report'
    assert parsed.dateref == valid_dateref

