
from datetime import date, datetime, timedelta

import pytest

from dagr.helpers import tz
from dagr.helpers import timeref



@pytest.fixture( scope = 'session', params = [
    '@now',

    # @H:MM and @HH:MM
    '@0:00',
    '@00:00',
    '@8:00',
    '@08:00',
    '@9:30',
    '@09:30',
    '@14:45',
    '@23:59',

    # @H and @HH
    '@0',
    '@00',
    '@8',
    '@08',
    '@9',
    '@09',
    '@14',
    '@23',

    # -Mm and friends,
    '+1h',
    '+1m',
    '+1h1m',
    '+12h',
    '+30m',
    '+10h30m',
    '+90m',
    '-1h',
    '-1m',
    '-1h1m',
    '-12h',
    '-30m',
    '-10h30m',
    '-90m',
] )
def valid_timeref_string( request ):
    return request.param



@pytest.fixture( scope = 'session', params = [
    # @HH:MM
    '@0:60',
    '@24:00',

    # @HH
    '@24',

    # @HH:MM bad syntax
    '@11:',

    # almost -Hh and friends
    '-1',
    '+1',
    '-99',
    '+99',
] )
def invalid_timeref_string( request ):
    return request.param



@pytest.fixture( scope = 'session', params = [
    '@then',
    '-',
    '+',
    '+me',
    '--option',
    '-o',
] )
def not_a_timeref_string( request ):
    return request.param



@pytest.mark.parametrize( 'refstring, expected_kwargs', [
    # absolute
    ( '@8:30', { 'hour': 8, 'minute': 30 } ),
    ( '@12', { 'hour': 12, 'minute': 0 } ),
    ( '@14:05', { 'hour': 14, 'minute': 5 } ),
    ( '@now', {} ),
] )
def test_timeref_to_datetime_absolute( refstring, expected_kwargs ):
    expected_kwargs.update( second = 0, microsecond = 0 )
    expected = tz.localize( datetime.now().replace( **expected_kwargs ) )
    assert expected == timeref.timeref_to_datetime( refstring )



@pytest.mark.parametrize( 'refstring, expected_kwargs', [
    # relative in the past
    ( '-4h5m', { 'hours': 4, 'minutes': 5 } ),
    ( '-2h', { 'hours': 2 } ),
    ( '-15m', { 'minutes': 15 } ),

    # relative in the future
    ( '+2h15m', { 'hours': 2, 'minutes': 15 } ),
    ( '+1h', { 'hours': 1 } ),
    ( '+5m', { 'minutes': 5 } ),
] )
def test_timeref_to_datetime( refstring, expected_kwargs ):
    expected = tz.localize( datetime.now().replace( second = 0, microsecond = 0 ) )
    if refstring.startswith( '-' ):
        expected = expected - timedelta( **expected_kwargs )
    else:
        expected = expected + timedelta( **expected_kwargs )
    assert expected == timeref.timeref_to_datetime( refstring )



def test_looks_like_valid_timeref( valid_timeref_string ):
    """looks_like_timeref should return True for all valid timeref strings"""
    assert timeref.looks_like_timeref( valid_timeref_string )
    assert timeref.looks_like_timeref( valid_timeref_string.upper() )



def test_looks_like_invalid_timeref( invalid_timeref_string ):
    """
    looks_like_timeref should return True for timeref strings that look like the user was trying,
    but are syntactically invalid or contain out of bounds values
    """
    assert timeref.looks_like_timeref( invalid_timeref_string )
    assert timeref.looks_like_timeref( invalid_timeref_string.upper() )



def test_looks_like_not_a_timeref( not_a_timeref_string ):
    """looks_like_timeref should return False for strings that aren't even close"""
    assert not timeref.looks_like_timeref( not_a_timeref_string )
    assert not timeref.looks_like_timeref( not_a_timeref_string.upper() )



def test_is_valid_timeref_valid_timeref( valid_timeref_string ):
    """is_valid_timeref should return True for valid timeref strings"""
    assert timeref.is_valid_timeref( valid_timeref_string )
    assert timeref.is_valid_timeref( valid_timeref_string.upper() )



def test_is_valid_timeref_invalid_timeref( invalid_timeref_string ):
    """is_valid_timeref should return False for invalid timeref strings"""
    assert not timeref.is_valid_timeref( invalid_timeref_string )
    assert not timeref.is_valid_timeref( invalid_timeref_string.upper() )



def test_is_valid_timeref_not_a_timeref( not_a_timeref_string ):
    """is_valid_timeref should return False for strings that aren't even close"""
    assert not timeref.is_valid_timeref( not_a_timeref_string )
    assert not timeref.is_valid_timeref( not_a_timeref_string.upper() )



def test_timeref_to_datetime_valid_timeref( valid_timeref_string ):
    """timeref_to_datetime should return a datetime for valid timeref strings"""
    assert isinstance( timeref.timeref_to_datetime( valid_timeref_string ), datetime )
    assert isinstance( timeref.timeref_to_datetime( valid_timeref_string.upper() ), datetime )



def test_timeref_to_datetime_invalid_timeref( invalid_timeref_string ):
    """timeref_to_datetime should raise InvalidTimeref for invalid timeref strings"""
    with pytest.raises( timeref.InvalidTimeref ):
        timeref.timeref_to_datetime( invalid_timeref_string )
    with pytest.raises( timeref.InvalidTimeref ):
        timeref.timeref_to_datetime( invalid_timeref_string.upper() )



def test_timeref_to_datetime_not_a_timeref( not_a_timeref_string ):
    """timeref_to_datetime should raise InvalidTimeref for strings that aren't even close"""
    with pytest.raises( timeref.InvalidTimeref ):
        timeref.timeref_to_datetime( not_a_timeref_string )
    with pytest.raises( timeref.InvalidTimeref ):
        timeref.timeref_to_datetime( not_a_timeref_string.upper() )



@pytest.mark.parametrize( 'refstring', [
    '@now',

    # @H:MM and @HH:MM
    '@0:00',
    '@00:00',
    '@8:00',
    '@08:00',
    '@9:30',
    '@09:30',
    '@14:45',
    '@23:59',

    # @H and @HH
    '@0',
    '@00',
    '@8',
    '@08',
    '@9',
    '@09',
    '@14',
    '@23',

    # -Mm and friends,
    # these are very hard to test relative to another date since they can cause the date
    # in the resultant datetime to be different than the original relative date, so we're
    # not gonig any farther from the current time than we have to. These unit tests will
    # likely fail if executed less than 1 minute away from midnight.
    '+1m',
    '-1m',
] )
def test_timeref_can_be_relative_to_another_day( refstring ):
    """the date portion of a timeref can be a specified datetime.date instead of today"""
    today = date.today()
    yesterday = today - timedelta( days = 1 )

    timeref_today = timeref.timeref_to_datetime( refstring )
    timeref_yesterday = timeref.timeref_to_datetime( refstring, relative_to = yesterday )

    assert today.year == timeref_today.year
    assert yesterday.year == timeref_yesterday.year

    assert today.month == timeref_today.month
    assert yesterday.month == timeref_yesterday.month

    assert today.day == timeref_today.day
    assert yesterday.day == timeref_yesterday.day

