
from datetime import date, timedelta
import pytest

from dagr.helpers import dateref



@pytest.fixture( scope = 'session', params = [
    '@today',
    '@yesterday',
    '@monday',
    '@mon',
    '@tuesday',
    '@tues',
    '@tue',
    '@wednesday',
    '@wed',
    '@thursday',
    '@thurs',
    '@thur',
    '@thu',
    '@friday',
    '@fri',
    '@saturday',
    '@sat',
    '@sunday',
    '@sun',

    # YYYYY-MM-DD
    '@2017-01-01',
    '@2017-01-31',
    '@2017-02-28',
    '@2017-03-31',
    '@2017-04-30',
    '@2017-05-31',
    '@2017-06-30',
    '@2017-07-31',
    '@2017-08-31',
    '@2017-09-30',
    '@2017-10-31',
    '@2017-11-30',
    '@2017-12-31',

    # leap years
    '@2016-02-29',

    # -Dd and friends,
    '-1d',
    '+1d',
    '-7d',
    '+7d',
    '-31d',
    '+31d',
] )
def valid_dateref_string( request ):
    return request.param



@pytest.fixture( scope = 'session', params = [
    # out of bounds month
    '@2017-00-01',
    '@2017-13-01',

    # out of bounds day
    '@2017-01-00',
    '@2017-01-32',
    '@2017-02-00',
    '@2017-02-29',
    '@2017-03-00',
    '@2017-03-32',
    '@2017-04-00',
    '@2017-04-31',
    '@2017-05-00',
    '@2017-05-32',
    '@2017-06-00',
    '@2017-06-31',
    '@2017-07-00',
    '@2017-07-32',
    '@2017-08-00',
    '@2017-08-32',
    '@2017-09-00',
    '@2017-09-31',
    '@2017-10-00',
    '@2017-10-32',
    '@2017-11-00',
    '@2017-11-31',
    '@2017-12-00',
    '@2017-12-32',

    # leap years out of bounds day
    '@2016-02-30',

    # not zero padded
    '@2017-02-3',
    '@2017-1-02',

    # non-standard weekday abbreviations
    '@mond',
    '@monda',
    '@tuesd',
    '@tuesda',
    '@wedn',
    '@wednes',
    '@wednesda',
    '@thursd',
    '@thursda',
    '@frid',
    '@frida',
    '@satu',
    '@satur',
    '@saturda',
    '@sund',
    '@sunda',

    # almost -Dd and friends
    '-1',
    '+1',
    '-31',
    '+31',
] )
def invalid_dateref_string( request ):
    return request.param



@pytest.fixture( scope = 'session', params = [
    '@then',
    '-',
    '+',
    '+me',
    '--option',
    '-o',
] )
def not_a_dateref_string( request ):
    return request.param



def test_dateref_special():
    assert dateref.dateref_to_date( '@today' ) == dateref.dateref_to_date( '@yesterday' ) + timedelta( days = 1 )



@pytest.mark.parametrize(
    'fake_today', [
    date( 2018, 8, 20 ),
    date( 2018, 8, 21 ),
    date( 2018, 8, 22 ),
    date( 2018, 8, 23 ),
    date( 2018, 8, 24 ),
    date( 2018, 8, 25 ),
    date( 2018, 8, 26 ),
] )
def test_dateref_weekdays( fake_today, monkeypatch ):
    """@mon, @tues, etc. should be interpreted in the same week as today"""
    monkeypatch.setattr( dateref, '_today', lambda: fake_today )
    assert date( 2018, 8, 20 ) == dateref.dateref_to_date( '@mon' )
    assert date( 2018, 8, 21 ) == dateref.dateref_to_date( '@tue' )
    assert date( 2018, 8, 22 ) == dateref.dateref_to_date( '@wed' )
    assert date( 2018, 8, 23 ) == dateref.dateref_to_date( '@thu' )
    assert date( 2018, 8, 24 ) == dateref.dateref_to_date( '@fri' )
    assert date( 2018, 8, 25 ) == dateref.dateref_to_date( '@sat' )
    assert date( 2018, 8, 26 ) == dateref.dateref_to_date( '@sun' )



@pytest.mark.parametrize( 'refstring, expected_args', [
    # absolute
    ( '@2018-07-01', ( 2018, 7, 1 ) ),
    ( '@2007-06-30', ( 2007, 6, 30 ) ),
] )
def test_dateref_absolute( refstring, expected_args ):
    expected = date( *expected_args )
    assert expected == dateref.dateref_to_date( refstring )



@pytest.mark.parametrize( 'refstring, expected_kwargs', [
    # relative in the past
    ( '-1d', { 'days': -1 } ),
    ( '-2d', { 'days': -2 } ),

    # relative in the future
    ( '-5d', { 'days': -5 } ),
    ( '-31d', { 'days': -31 } ),
] )
def test_dateref_relative( refstring, expected_kwargs ):
    expected = date.today() + timedelta( **expected_kwargs )
    assert expected == dateref.dateref_to_date( refstring )



def test_looks_like_valid_dateref( valid_dateref_string ):
    """looks_like_dateref should return True for all valid dateref strings"""
    assert dateref.looks_like_dateref( valid_dateref_string )
    assert dateref.looks_like_dateref( valid_dateref_string.upper() )



def test_looks_like_invalid_dateref( invalid_dateref_string ):
    """
    looks_like_dateref should return True for dateref strings that look like the user was trying,
    but are syntactically invalid or contain out of bounds values
    """
    assert dateref.looks_like_dateref( invalid_dateref_string )
    assert dateref.looks_like_dateref( invalid_dateref_string.upper() )



def test_looks_like_not_a_dateref( not_a_dateref_string ):
    """looks_like_dateref should return False for strings that aren't even close"""
    assert not dateref.looks_like_dateref( not_a_dateref_string )
    assert not dateref.looks_like_dateref( not_a_dateref_string.upper() )



def test_is_valid_dateref_valid_dateref( valid_dateref_string ):
    """is_valid_dateref should return True for valid dateref strings"""
    assert dateref.is_valid_dateref( valid_dateref_string )
    assert dateref.is_valid_dateref( valid_dateref_string.upper() )



def test_is_valid_dateref_invalid_dateref( invalid_dateref_string ):
    """is_valid_dateref should return False for invalid dateref strings"""
    assert not dateref.is_valid_dateref( invalid_dateref_string )
    assert not dateref.is_valid_dateref( invalid_dateref_string.upper() )



def test_is_valid_dateref_not_a_dateref( not_a_dateref_string ):
    """is_valid_dateref should return False for strings that aren't even close"""
    assert not dateref.is_valid_dateref( not_a_dateref_string )
    assert not dateref.is_valid_dateref( not_a_dateref_string.upper() )



def test_dateref_to_date_valid_dateref( valid_dateref_string ):
    """dateref_to_date should return a date for valid dateref strings"""
    assert isinstance( dateref.dateref_to_date( valid_dateref_string ), date )
    assert isinstance( dateref.dateref_to_date( valid_dateref_string.upper() ), date )



def test_dateref_to_date_invalid_dateref( invalid_dateref_string ):
    """dateref_to_date should raise InvalidDateref for invalid dateref strings"""
    with pytest.raises( dateref.InvalidDateref ):
        dateref.dateref_to_date( invalid_dateref_string )
    with pytest.raises( dateref.InvalidDateref ):
        dateref.dateref_to_date( invalid_dateref_string.upper() )



def test_dateref_to_date_not_a_dateref( not_a_dateref_string ):
    """dateref_to_date should raise InvalidDateref for strings that aren't even close"""
    with pytest.raises( dateref.InvalidDateref ):
        dateref.dateref_to_date( not_a_dateref_string )
    with pytest.raises( dateref.InvalidDateref ):
        dateref.dateref_to_date( not_a_dateref_string.upper() )

