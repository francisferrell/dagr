
from datetime import datetime, timedelta
from pytest import fixture

from dagr.models import Task



@fixture( scope = 'session' )
def time_one():
    now = datetime.now()
    return datetime(
        year = now.year,
        month = now.month,
        day = now.day,
        hour = now.hour,
        minute = now.minute,
        second = 14,
        microsecond = 1234,
        tzinfo = now.tzinfo
    )



@fixture( scope = 'session' )
def time_two( time_one ):
    return time_one + timedelta( minutes = 1 )



def test_tasks_are_comparable_by_start( time_one, time_two ):
    t1 = Task( start = time_one, description = '' )
    t1a = Task( start = time_one, description = 'a' )
    t1z = Task( start = time_one, description = 'z' )

    t2 = Task( start = time_two, description = '' )
    t2a = Task( start = time_two, description = 'a' )
    t2z = Task( start = time_two, description = 'z' )

    for lhs in ( t1, t1a, t1z ):
        for rhs in ( t2, t2a, t2z ):
            assert lhs < rhs

    for lhs in ( t1, t1a, t1z ):
        for rhs in ( t1, t1a, t1z, t2, t2a, t2z ):
            assert lhs <= rhs

    for lhs in ( t1, t1a, t1z ):
        for rhs in ( t1, t1a, t1z ):
            assert lhs == rhs

    for lhs in ( t2, t2a, t2z ):
        for rhs in ( t2, t2a, t2z ):
            assert lhs == rhs

    for lhs in ( t1, t1a, t1z, t2, t2a, t2z ):
        for rhs in ( t1, t1a, t1z ):
            assert lhs >= rhs

    for lhs in ( t2, t2a, t2z ):
        for rhs in ( t1, t1a, t1z ):
            assert lhs > rhs



def test_tasks_ignore_seconds( time_one ):
    t = Task( start = time_one, description = '' )
    rounded = datetime(
        year = time_one.year,
        month = time_one.month,
        day = time_one.day,
        hour = time_one.hour,
        minute = time_one.minute,
        second = 0,
        microsecond = 0,
        tzinfo = time_one.tzinfo
    )

    assert t.start != time_one
    assert t.start == rounded



def test_tasks_compare_to_datetimes():
    """
    a datetime should be comprable to a Task by the task's start time
    """
    time1 = datetime( 2018, 8, 25, 13, 5 )
    time2 = datetime( 2018, 8, 25, 13, 6 )
    task = Task( time2, 'test' )

    assert time1 < task
    assert time1 <= task
    assert not time1 == task
    assert time1 != task
    assert not time1 > task
    assert not time1 >= task

    assert not task < time1
    assert not task <= time1
    assert not task == time1
    assert task != time1
    assert task > time1
    assert task >= time1

    assert not time2 < task
    assert time2 <= task
    assert time2 == task
    assert not time2 != task
    assert not time2 > task
    assert time2 >= task

    assert not task < time2
    assert task <= time2
    assert task == time2
    assert not task != time2
    assert not task > time2
    assert task >= time2

