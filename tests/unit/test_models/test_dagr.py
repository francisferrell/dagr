
from datetime import datetime
from itertools import permutations
import pytest

from dagr.models.dagr import Dagr
from dagr.models.task import Task



@pytest.fixture( params = permutations( [ 1, 2, 3, 4 ] ) )
def any_sequence_of_four_numbers( request ):
    """
    every permutation of 4 consecutive numbers
    """
    return request.param



def test_init_sorted( any_sequence_of_four_numbers ):
    """
    a Dagr starts sorted, if an iterable was provided to __init__
    """
    sorted_list = Dagr( any_sequence_of_four_numbers )
    assert sorted_list[0] < sorted_list[1]
    assert sorted_list[1] < sorted_list[2]
    assert sorted_list[2] < sorted_list[3]



def test_append_stays_sorted( any_sequence_of_four_numbers ):
    """
    growing a Dagr with append() doesn't violate the sorted principle
    """
    sorted_list = Dagr( [ any_sequence_of_four_numbers[0] ] )

    sorted_list.append( any_sequence_of_four_numbers[1] )
    assert sorted_list[0] < sorted_list[1]

    sorted_list.append( any_sequence_of_four_numbers[2] )
    assert sorted_list[0] < sorted_list[1]
    assert sorted_list[1] < sorted_list[2]

    sorted_list.append( any_sequence_of_four_numbers[3] )
    assert sorted_list[0] < sorted_list[1]
    assert sorted_list[1] < sorted_list[2]
    assert sorted_list[2] < sorted_list[3]



@pytest.mark.parametrize( 'i', range(4) )
def test_extend_stays_sorted( any_sequence_of_four_numbers, i ):
    """
    growing a Dagr with extend() doesn't violate the sorted principle
    """
    # partition the test fixture at every possible ``i``, initializing the
    # Dagr with the first half and extend()'ing with the second half
    sorted_list = Dagr( any_sequence_of_four_numbers[0:i] )
    sorted_list.extend( any_sequence_of_four_numbers[i:] )
    assert sorted_list[0] < sorted_list[1]
    assert sorted_list[1] < sorted_list[2]
    assert sorted_list[2] < sorted_list[3]



@pytest.mark.parametrize( 'pos', range(4) )
def test_insert_stays_sorted( any_sequence_of_four_numbers, pos ):
    """
    growing a Dagr with insert() doesn't violate the sorted principle
    """
    # start with the Dagr initilized with the first 3 test fixture elements,
    # insert the 4th test fixture element at any valid position
    sorted_list = Dagr( any_sequence_of_four_numbers[:-1] )
    sorted_list.insert( pos, any_sequence_of_four_numbers[-1] )
    assert sorted_list[0] < sorted_list[1]
    assert sorted_list[1] < sorted_list[2]
    assert sorted_list[2] < sorted_list[3]



def test_index_assignment_stays_sorted():
    """
    changing the value of an element in a Dagr doesn't violate the sorted principle, because
    it is not allowed.
    """
    # start with a Dagr initialized with the first 3 elements
    # assign the remaining element via []= at any valid position
    sorted_list = Dagr( [ 1, 2, 3 ] )
    with pytest.raises( NotImplementedError ):
        sorted_list[1] = 4
    assert sorted_list[0] < sorted_list[1]
    assert sorted_list[1] < sorted_list[2]



def test_slice_assignment_stays_sorted( any_sequence_of_four_numbers ):
    """
    growing a Dagr with slice assignment doesn't violate the sorted principle, because it is
    not allowed.
    """
    sorted_list = Dagr( [ 1, 2, 3 ] )

    with pytest.raises( NotImplementedError ):
        sorted_list[3:] = [ 4, 5 ]
    assert 3 == len( sorted_list )
    assert 1 == sorted_list[0]
    assert 2 == sorted_list[1]
    assert 3 == sorted_list[2]



def test_insert_replaces():
    """
    when inserting a new item that equals an existing item, the new item should replace the existing
    """
    eight_am = datetime( 2018, 8, 25, 8, 0 )
    eight_fifteen = datetime( 2018, 8, 25, 8, 15 )
    nine_o_five = datetime( 2018, 8, 25, 9, 5 )

    sorted_list = Dagr( [
        Task( eight_am, 'read email' ),
        Task( eight_fifteen, 'fix bugs', 'TKT-02' ),
        Task( nine_o_five, 'add features', 'TKT-03' ),
    ] )

    description = 'fix other bugs'
    ticket = 'TKT-04'
    sorted_list.append( Task( eight_fifteen, description, ticket ) )

    assert 3 == len( sorted_list )
    assert eight_am == sorted_list[0].start
    assert eight_fifteen == sorted_list[1].start
    assert nine_o_five == sorted_list[2].start
    assert description == sorted_list[1].description
    assert ticket == sorted_list[1].ticket



def test_index_by_datetime():
    """
    we should be able to use a datetime in index notation to find a Task that starts at that time
    """
    eight_am = datetime( 2018, 8, 25, 8, 0 )
    eight_fifteen = datetime( 2018, 8, 25, 8, 15 )
    nine_o_five = datetime( 2018, 8, 25, 9, 5 )

    dagr = Dagr( [
        Task( eight_am, 'read email' ),
        Task( eight_fifteen, 'fix bugs', 'TKT-02' ),
        Task( nine_o_five, 'add features', 'TKT-03' ),
    ] )

    assert 'read email' == dagr[eight_am].description
    assert 'fix bugs' == dagr[eight_fifteen].description
    assert 'add features' == dagr[nine_o_five].description
    with pytest.raises( IndexError ):
        dagr[datetime( 2018, 8, 25, 8, 30 )].description
    assert 'read email' == dagr[eight_am.replace( second = 1, microsecond = 1 )].description



def test_delete_by_datetime():
    """
    we should be able to use a datetime in index notation to delete a Task that starts at that time
    """
    eight_am = datetime( 2018, 8, 25, 8, 0 )
    eight_fifteen = datetime( 2018, 8, 25, 8, 15 )
    nine_o_five = datetime( 2018, 8, 25, 9, 5 )

    dagr = Dagr( [
        Task( eight_am, 'read email' ),
        Task( eight_fifteen, 'fix bugs', 'TKT-02' ),
        Task( nine_o_five, 'add features', 'TKT-03' ),
    ] )

    assert 3 == len( dagr )
    assert 'read email' == dagr[0].description
    assert 'fix bugs' == dagr[1].description
    assert 'add features' == dagr[2].description

    del dagr[eight_fifteen]

    assert 2 == len( dagr )
    assert 'read email' == dagr[0].description
    assert 'add features' == dagr[1].description

    with pytest.raises( IndexError ):
        del dagr[eight_fifteen]

