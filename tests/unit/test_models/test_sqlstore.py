
from datetime import date, timedelta

import pytest

from dagr.helpers import tz
from dagr.models import Dagr, Task, End, SqlStore



def today( hour, minute, second ):
    """instantiates datetime objects with the date portion set to today"""
    d = date.today()
    return tz.local_datetime( d.year, d.month, d.day, hour, minute, second )



def yesterday( *args ):
    """instantiates datetime objects with the date portion set to yesterday"""
    return today( *args ) - timedelta( days = 1 )



@pytest.fixture(
    params = [ today, yesterday ],
    ids = [ 'today', 'yesterday' ],
)
def workday( request ):
    on_date = request.param
    return Dagr( [
        Task( start = on_date( 8, 0, 0 ), description = 'make coffee' ),
        Task( start = on_date( 8, 9, 0 ), description = 'read email' ),
        Task( start = on_date( 8, 19, 17 ), description = 'implement dagr', ticket = 'WKL-1' ),
        Task( start = on_date( 11, 55, 30 ), description = 'lunch' ),
        Task( start = on_date( 12, 47, 56 ), description = 'fix dagr bugs', ticket = 'WKL-2' ),
        End( start = on_date( 17, 0, 0 ) )
    ] )



@pytest.fixture
def sql():
    return SqlStore( path = ':memory:' )



def test_scenario_workday( sql, workday ):
    # the purpose here is to test the dataset iteratively, hitting each step that the user would,
    # as the dataset is built across the day. That is, we want to test workday[0] and workday[0],
    # workday[1], and also workday[0], workday[1], workday[2], and so on until we've tested the
    # entire workday. it doesn't seem that pytest fixtures let us do this elegantly.
    for i in range( 1, len( workday ) + 1 ):
        # take an i-sized sub-list, always starting from the front of the list
        expected_tasks = workday[0:i]
        when = workday[0].start.date()

        sql.save( expected_tasks )

        actual_tasks = sql.load( when )

        assert isinstance( actual_tasks, Dagr )
        assert len( expected_tasks ) == len( actual_tasks )

        for expected, actual in zip( expected_tasks, actual_tasks ):
            assert type( expected ) is type( actual )
            assert expected.start == actual.start
            assert expected.description == actual.description
            assert expected.ticket == actual.ticket



def test_remove_middle( sql, workday ):
    """we should be able to remove an entry in the middle of the day"""
    when = workday[0].start.date()
    assert 'make coffee' == workday[0].description
    assert 'read email' == workday[1].description
    assert 'implement dagr' == workday[2].description
    sql.save( workday )

    del workday[1]
    sql.save( workday )

    loaded_tasks = sql.load( when )
    assert len( workday ) == len( loaded_tasks )
    assert 'make coffee' == loaded_tasks[0].description
    assert 'implement dagr' == loaded_tasks[1].description

