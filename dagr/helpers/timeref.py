"""
A timeref is a string describing a time and having the following characteristics:

    * human friendly to read and write
    * absolutely anchored on any given date (starts with ``@``)
    * relative to right now on any given date (starts with ``+`` or ``-``)
    * uses 24-hour time notation
    * minute accuracy is sufficient, seconds will always be ``0``
    * relative references are not intended for offset in days, only hours and minutes may be
      added/subtracted

The best way to understand the string representation is by example. All of these are valid
timerefs:

    * ``@8:30``
    * ``@12``
    * ``@14:05``
    * ``@14``
    * ``+2h15m``
    * ``+1h``
    * ``+5m``
    * ``-4h5m``
    * ``-2h``
    * ``-15m``
"""

from datetime import date, timedelta
import re

from .. import DagrError
from . import tz



class InvalidTimeref( DagrError ):
    pass



_rx_looks_like_timeref = re.compile( r'^(@now$|@\d|[-+]\d)', re.IGNORECASE )



def looks_like_timeref( candidate ):
    """
    Determine if ``candidate`` is trying to be a valid timeref string.

    This method is useful for checking if the user was trying to supply a timeref string. It
    wouldn't be friendly to the user if we were to strictly validate everything as either "a
    completely valid timeref" or "not interesting". For example, if the user provides ``@9:66``,
    a typo, it would be more useful to report an error than to ignore it and treat it as part of
    the description of a Task.

    If this method returns ``False``, then ``timeref_to_datetime`` will definitely raise
    ``InvalidTimeref``. However, if this methods returns ``True``, then ``timeref_to_datetime``
    may or may not raise.

    Args:
        candidate (str): the purported timeref string

    Returns:
        bool
    """
    return _rx_looks_like_timeref.match( candidate ) is not None



_rx_valid_timeref = re.compile( '^(?:{})$'.format( '|'.join( (
    # dead simple special value
    #
    # is_now group will be the string '@now'
    r'(?P<is_now>@now)',

    # matches '@H', '@HH', '@H:MM', and '@HH:MM'
    #
    # bounds checked hour value will be in the absolute_hour group
    # bounds checked minute value will be in the absolute_minute group
    #   absolute_minute will be None if it was omitted
    r'(?P<is_absolute>@)(?P<absolute_hour>0?\d|1\d|2[0123])(?::(?P<absolute_minute>[012345]\d))?',

    # matches '-Hh', '-Mm', '-HhMm', and the '+...' varieties of each
    #
    # hour value will be in the relative_hour group
    # minute value will be in either the relative_minute_optional or relative_minute_required group
    #   the regex guarantees us that one or both of relative_minute_* will be None.
    #   they will not both be non-None
    #
    # DOES NOT BOUNDS CHECK, but really there are no bounds that matter.
    # While we don't really want to match '-0m' or '-0h', the result will work as expected.
    # It's just not very useful since we have the special value '@now', which is more clear and
    # explicit.
    #
    # HERE BE DRAGONS OF THE REGEX VARIETY!!
    #
    # for a regex to match AB, ABC, and AC, but not A, we have to use | to branch the B, C, and
    # BC use cases, which means group capturing becomes complicated
    #
    # we can decrease, but not eliminate this complexity by using the (?(cond)true|false) construct
    # so that at least the value of B is always in the same group capture. The irreducible
    # complexity is that we still have a branch for C that will result in its value being in one of
    # two gropus depending on whether or not B was omitted.
    #
    # specifically, we use the conditional construct thusly: the condition is whether or not 'Hh'
    # was found. If it was, then 'Mm' is optional. If it was not, then 'Mm' is required.
    r'(?P<is_relative>[-+])(?:(?P<relative_hour>\d+)h)?(?(relative_hour)(?:(?P<relative_minute_optional>\d+)m)?|(?:(?P<relative_minute_required>\d+)m))',
) ) ), re.IGNORECASE )



def is_valid_timeref( candidate ):
    """
    Determine whether or not ``candidate`` is a fully valid timeref string.

    If this methods returns ``True`` then ``timeref_to_datetime`` will return a ``datetime``. If
    this method returns ``False`` then ``timeref_to_datetime`` will raise ``InvalidTimeref``.

    Args:
        candidate (str): the purported timeref string

    Returns:
        bool
    """
    # unlike dateref, this regex does all the bounds checking we care about
    # no need to try a full parse and convert the exception to False
    return _rx_valid_timeref.match( candidate ) is not None



def timeref_to_datetime( refstring, relative_to = date.today() ):
    """
    Convert a timeref string to its actual ``datetime``. If ``refstring`` is relative, it will be
    evaluated relative to ``datetime.now()`` as of when this method is called. If it is absolute,
    it will occur on ``date.today()``.

    Args:
        refstring (str): a valid timeref string
        relative_to (datetime.date): another date other than today to use as the relative reference
            point. Note that the hour and minute will still be taken from ``datetime.now()``

    Returns:
        datetime

    Raises:
        InvalidTimeref: if ``refstring`` is not a valid timeref string
    """
    match = _rx_valid_timeref.match( refstring )
    if match is None:
        raise InvalidTimeref( 'invalid timeref string: {!r}', refstring )

    _now = tz.local_now()
    now = tz.local_datetime(
        year = relative_to.year,
        month = relative_to.month,
        day = relative_to.day,
        hour = _now.hour,
        minute = _now.minute,
        second = 0,
        microsecond = 0
    )

    # (?P<is_now>@now)
    if match.group( 'is_now' ) is not None:
        return now

    # (?P<is_absolute>@)(?P<absolute_hour>0?\d|1\d|2[0123])(?::(?P<absolute_minute>[012345]\d))?
    # minutes are optional and should default to 0
    if match.group( 'is_absolute' ) is not None:
        return now.replace(
            hour = int( match.group( 'absolute_hour' ), base = 10 ),
            minute = int( match.group( 'absolute_minute' ) or '0', base = 10 )
        )

    # (?P<is_relative>[-+])(?:(?P<relative_hour>\d+)h)?(?(relative_hour)(?:(?P<relative_minute_optional>\d+)m)?|(?:(?P<relative_minute_required>\d+)m))
    # this one is a bit complicated:
    #   is_relative contains the sign -/+
    #   hours and minutes should each default to 0 if missing
    #   because of the regex constraint of unique group names, and our constraint of at least one of
    #     hours or minutes is required, minutes value may be in one of two groups
    if match.group( 'is_relative' ) is not None:
        delta = timedelta(
            hours = int( match.group( 'relative_hour' ) or '0', base = 10 ),
            minutes = int( match.group( 'relative_minute_optional' ) or match.group( 'relative_minute_required' ) or '0', base = 10 )
        )

        if match.group( 'is_relative' ) == '-':
            return now - delta
        elif match.group( 'is_relative' ) == '+':
            return now + delta



__all__ = [
    'looks_like_timeref',
    'is_valid_timeref',
    'timeref_to_datetime',
]

