"""
provides integration with Jira.
"""

import stat
from sys import stderr

from jira import JIRA, JIRAError

from .. import DagrError
from .config import ConfigError
from .tz import utc



class IssueNotFound( DagrError ):
    """
    The named issue was not found in Jira.
    """



class InsecureConfig( DagrError ):
    """
    The configuration file is insecure.
    """



class Jira:

    def __init__( self, config ):
        if config.jira is None:
            raise ConfigError( config, 'jira is a required section to connect to jira' )
        if 'host' not in config.jira:
            raise ConfigError( config, 'jira host is a required field' )
        if 'user' not in config.jira:
            raise ConfigError( config, 'jira user is a required field' )
        if 'password' not in config.jira:
            raise ConfigError( config, 'jira password is a required field' )

        if config.path.exists():
            bits = config.path.stat()
            if bits.st_mode & stat.S_IRWXG != 0 or bits.st_mode & stat.S_IRWXO != 0:
                raise InsecureConfig( 'config file with jira credentials can be accessed by other users! consider chmod 600 {}'.format( config.path ) )

        self._jira = JIRA( config.jira['host'], basic_auth = ( config.jira['user'], config.jira['password'] ) )


    def get_issue( self, issue, fields = None ):
        """
        Get the named ``issue`` from Jira.

        Args:
            fields: passed directly to python-jira's ``JIRA.issue()``

        Returns
            jira.Issue: provied by python-jira
        """
        try:
            return self._jira.issue( issue, fields = fields )
        except JIRAError as err:
            if err.response.status_code == 404:
                raise IssueNotFound( 'no such issue: {issue}', issue = issue )
            else:
                raise err


    def upload_worklog( self, ticket, start, duration ):
        """
        Log work in Jira.

        Args:
            ticket (str): the ticket number ("issue" in Jira lingo)
            start (datetime): the start time, must be timezeon-aware, will be converted to UTC
                for you
            duration (timedelta): the length of time worked
        """
        return self._jira.add_worklog(
            ticket,
            timeSpentSeconds = int( duration.total_seconds() ),
            started = start.astimezone( utc )
        )

