"""
Provides runtime parameters from the config file.
"""

from configparser import ConfigParser
from os import environ
from pathlib import Path

from .. import DagrError



class ConfigError( DagrError ):
    """
    Any semantic error in the conf file, which the user should be notified so they can fix it
    """

    def __init__( self, config, *args, **kwargs ):
        self.config = config
        super().__init__( *args, **kwargs )



class Config( ConfigParser ):
    """
    Convenience wrapper of the standard library ``configparser.ConfigParser``.

    Also provides each section as an attribute on the object so you don't need to use index notation
    to access them.
    """

    config_home = Path.home() / '.config' / 'dagr'
    data_home = Path.home() / '.local' / 'share' / 'dagr'


    def __init__( self ):
        super().__init__()
        self.path = self.config_home / 'config.ini'
        self.read( str( self.path ) )

        self.jira = None

        for s in self.sections():
            setattr( self, s, self[s] )



if 'XDG_CONFIG_HOME' in environ:
    Config.config_home = Path( environ['XDG_CONFIG_HOME'] ) / 'dagr'

if 'XDG_DATA_HOME' in environ:
    Config.data_home = Path( environ['XDG_DATA_HOME'] ) / 'dagr'

