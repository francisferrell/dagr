"""
Much like a timeref, a dateref is an absolute or relative reference to a date.

The best way to understand the string representation is by example. All of these are valid
daterefs:

    * ``@2017-02-26``
    * ``@today``
    * ``@mon``
    * ``@thursday``
    * ``+1d``
    * ``-5d``

Absolute date references must be the ``@`` symbol followed by a date in ISO 8601 format
(``yyyy-mm-dd``, with zero padding), or ``@`` followed by a known keyword:

    * ``@today``
    * ``@yesterday``
    * ``@monday`` or ``@mon``
    * ``@tuesday`` or ``@tues`` or ``@tue``
    * ``@wednesday`` or ``@wed``
    * ``@thursday`` or ``@thurs`` or ``@thur`` or ``@thu``
    * ``@friday`` or ``@fri``
    * ``@saturday`` or ``@sat``
    * ``@sunday`` or ``@sun``

The weekday names evaluate in the week of today at the time of evaluation. That is, one of them will
always be the same as ``@today`` and the others will be fewer than 7 days away from today, either in
the past or in the future.

Relative references must start with ``-`` or ``+``, followed by a number, and suffixed with ``d``.
"""

from datetime import date, timedelta
import re

from .. import DagrError



class InvalidDateref( DagrError ):
    pass



_rx_looks_like_dateref = re.compile( '^({})'.format( '|'.join( (
    r'@(tod|yest)',
    r'@(mo|tu|we|thu|fr|sa|su)',
    r'@\d\d{2,4}-\d',
    r'[-+]\d',
) ) ), re.IGNORECASE )



def looks_like_dateref( candidate ):
    """
    Determine if ``candidate`` is trying to be a valid dateref string.

    This method is useful for checking if the user was trying to supply a dateref string. It
    wouldn't be friendly to the user if we were to strictly validate everything as either "a
    completely valid dateref" or "not valid, so I'm ignoring it". For example, if the user provides
    ``@2017-01-0``, a typo, it would be more useful to report an error than to ignore it and do
    something surprising.

    If this method returns ``False``, then ``dateref_to_date`` will definitely raise
    ``InvalidDateref``.  However, if this methods returns ``True``, then ``dateref_to_date`` may
    or may not raise.

    Args:
        candidate (str): the purported dateref string

    Returns:
        bool
    """
    return _rx_looks_like_dateref.match( candidate ) is not None



_rx_valid_dateref = re.compile( '^(?:{})$'.format( '|'.join( (
    # matches '@today' and '@yesterday'
    #
    # is_special group will be not None
    # special group will be the text value
    r'(?P<is_special>@(?P<special>today|yesterday))',

    # matches all of the weekday names
    #
    # is_weekday group will be not None
    # weekday group will be the text value
    # ``@monday`` or ``@mon``
    # ``@tuesday`` or ``@tues`` or ``@tue``
    # ``@wednesday`` or ``@wed``
    # ``@thursday`` or ``@thurs`` or ``@thur`` or ``@thu``
    # ``@friday`` or ``@fri``
    # ``@saturday`` or ``@sat``
    # ``@sunday`` or ``@sun``
    r'(?P<is_weekday>@(?P<weekday>{}))'.format( '|'.join( (
        r'mon(day)?',
        r'tue(s(day)?)?',
        r'wed(nesday)?',
        r'thu(r(s(day)?)?)?',
        r'fri(day)?',
        r'sat(urday)?',
        r'sun(day)?',
    ) ) ),

    # matches '@YYYY-MM-DD'
    #
    # DOES NOT BOUNDS CHECK
    #
    # is_absolute group will be not None
    # absolute_year will be the year value
    # absolute_month will be the month value
    # absolute_day will be the day value
    r'(?P<is_absolute>@)(?P<absolute_year>\d{4})-(?P<absolute_month>\d{2})-(?P<absolute_day>\d{2})',

    # matches '-Dd', and '+Dd'
    #
    # is_relative group will be '-' or '+'
    # relative_days group will be the D value
    #
    # DOES NOT BOUNDS CHECK, but really there are no bounds that matter.
    # While we don't really want to match '-0d', the result will work as expected.
    # It's just not very useful since we have the special value '@today', which is clearer
    r'(?P<is_relative>[-+])(?:(?P<relative_days>\d+)d)',
) ) ), re.IGNORECASE )



def is_valid_dateref( candidate ):
    """
    Determine whether or not ``candidate`` is a fully valid dateref string.

    If this methods returns ``True`` then ``dateref_to_date`` will return a ``date``. If this method
    returns ``False`` then ``dateref_to_date`` will raise ``InvalidDateref``.

    Args:
        candidate (str): the purported dateref string

    Returns:
        bool
    """

    # while I'd like to validate for out-of-bounds values without resorting to attempting to
    # instantiate a date, and while I'd *love* to elegantly bounds check in the regex itself,
    # it's just not practical in the case of the irregularities of the calendar.
    try:
        dateref_to_date( candidate )
    except InvalidDateref:
        return False
    else:
        return True



def _today():
    """
    this exists to be mocked by unit tests.
    """
    return date.today()



def dateref_to_date( refstring ):
    """
    Convert a dateref string to its actual ``date``. If ``refstring`` is relative, it will be
    evaluated relative to ``date.today()`` as of when this method is called.

    Args:
        refstring (str): a valid dateref string

    Returns:
        date

    Raises:
        InvalidDateref: if ``refstring`` is not a valid dateref string
    """
    match = _rx_valid_dateref.match( refstring )
    if match is None:
        raise InvalidDateref( 'invalid dateref string: {!r}', refstring )

    today = _today()

    # matches '@today' and '@yesterday'
    #
    # is_special group will be not None
    # special group will be the text value
    if match.group( 'is_special' ) is not None:
        special = match.group( 'special' ).lower()
        if special == 'today':
            return today
        if special == 'yesterday':
            return today - timedelta( days = 1 )

    # matches all of the weekday names
    #
    # is_weekday group will be not None
    # weekday group will be the text value
    if match.group( 'is_weekday' ) is not None:
        weekday = match.group( 'weekday' ).lower()
        if weekday.startswith( 'mon' ):
            return today - timedelta( days = today.weekday() - 0 )
        if weekday.startswith( 'tue' ):
            return today - timedelta( days = today.weekday() - 1 )
        if weekday.startswith( 'wed' ):
            return today - timedelta( days = today.weekday() - 2 )
        if weekday.startswith( 'thu' ):
            return today - timedelta( days = today.weekday() - 3 )
        if weekday.startswith( 'fri' ):
            return today - timedelta( days = today.weekday() - 4 )
        if weekday.startswith( 'sat' ):
            return today - timedelta( days = today.weekday() - 5 )
        if weekday.startswith( 'sun' ):
            return today - timedelta( days = today.weekday() - 6 )

    # matches '@YYYY-MM-DD'
    #
    # REGEX DOES NO BOUNDS CHECKING
    #
    # is_absolute group will be not None
    # absolute_year will be the year value
    # absolute_month will be the month value
    # absolute_day will be the day value
    if match.group( 'is_absolute' ):
        try:
            return date(
                year = int( match.group( 'absolute_year' ), base = 10 ),
                month = int( match.group( 'absolute_month' ), base = 10 ),
                day = int( match.group( 'absolute_day' ), base = 10 ),
            )
        except ValueError:
            raise InvalidDateref( 'invalid dateref string: {!r}', refstring ) from None

    # matches '-Dd', and '+Dd'
    #
    # is_relative group will be '-' or '+'
    # relative_days group will be the D value
    if match.group( 'is_relative' ):
        delta = timedelta( days = int( match.group( 'relative_days' ), base = 10 ) )

        if match.group( 'is_relative' ) == '-':
            return today - delta
        if match.group( 'is_relative' ) == '+':
            return today + delta



__all__ = [
    'looks_like_dateref',
    'is_valid_dateref',
    'dateref_to_date',
]

