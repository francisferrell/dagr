"""
Make sure every instance of ``datetime`` in the entire application is timezone-aware
using the local timezone as provided by the ``tzlocal`` package.
"""

from datetime import datetime

from pytz import utc
from tzlocal import get_localzone



_zone = None

def localize( *args, **kwargs ):
    """
    wraps ``tzlocal.get_localzone().localize``
    """
    global _zone
    if _zone is None:
        _zone = get_localzone()

    return _zone.localize( *args, **kwargs )



def local_now():
    """
    ``datetime.now()``, aware of the system local timezone
    """
    return localize( datetime.now() )



def local_fromtimestamp( value ):
    """
    ``datetime.fromtimestamp( value )``, aware of the system local timezone

    Args:
        value (int): the seconds since epoch
    """
    return localize( datetime.fromtimestamp( value ) )



def local_datetime( *args, **kwargs ):
    """
    ``datetime( *args, **kwargs )``, aware of the system local timezone

    Args:
        args (\*args): passed directly to ``datetime()``
        kwargs (\*\*kwargs): passed directly to ``datetime()``
    """
    return localize( datetime( *args, **kwargs ) )

