"""
All output produced by ``dagr`` is implemented using jinja2 templates. This module provides all the
moving parts for template rendering.

If you want to write your own view templates for ``dagr`` and are wondering what all you can utilize
in your template, you're in the right palce.

Methods whose name start with ``filter_`` are added to the view as jinja2 filters (but with the
``filter_`` removed). Of course, you can use all the filters provided by the standard jinja2
rendering environment.
"""

from datetime import timedelta
import inspect
import sys

from jinja2 import Environment, ChoiceLoader, FileSystemLoader, PackageLoader
from jinja2 import TemplateNotFound # for the importing code's convenience

from ..models import End
from .config import Config
from . import tz



def filter_timefmt( datetime, strip = True ):
    """
    format the time portion of a datetime as ``H:MM``.

    Args:
        strip (bool): override with ``False`` to format as ``HH:MM``.

    Examples::

        {{ task.start | timefmt }}
    """
    value = datetime.strftime( '%H:%M' )
    if strip:
        return value.lstrip( '0' )
    else:
        return value



def filter_datefmt( datetime, fmt = '%Y-%m-%d' ):
    """
    format the date portion of a datetime or date as ``YYYY-MM-DD``.

    Args:
        fmt (str): override the format produced, accepts anything valid for ``date.strftime()``

    Examples::

        {{ task.start | datefmt }}

        {{ args.dateref | datefmt('%x') }}
    """
    return datetime.strftime( fmt )



def filter_durationfmt( delta ):
    """
    format a timedelta as ``XhYm``.

    Examples::

        {{ task.duration | durationfmt }}
    """
    seconds = int( delta.total_seconds() )
    hours, seconds = divmod( seconds, 60 * 60 )
    minutes, seconds = divmod( seconds, 60 )
    parts = list()
    if hours > 0:
        parts.append( '{:d}h'.format( hours ) )
    parts.append( '{:d}m'.format( minutes ) )
    return ' '.join( parts )



def filter_sumdurations( tasks ):
    """
    sum the ``duration`` attribute of ``tasks``.

    Examples::

        {{ tasks | sumdurations }}
    """
    return sum( ( task.duration for task in tasks ), timedelta( seconds = 0 ) )



def filter_justify( string, width ):
    """
    left-justify or right-justify a string at a specified width

    Args:
        width (int): character width to fill; positive for right-justify, negative for left-justify.
            ``0`` to leave ``string`` unmodified.

    Examples::

        {{ task.start | timefmt | justify(5) }}
    """
    if width > 0:
        return string.rjust( width )
    elif width < 0:
        return string.ljust( abs( width ) )
    else:
        return string



ESC = '\x1b['
RESET = ESC + '0m'

def wrap_style( content, code ):
    """
    wrap ``content`` in the ANSI escape sequences for color or decoration ``code``
    """
    return '{ESC}{code}m{content}{RESET}'.format(
        ESC = ESC,
        code = code,
        content = content,
        RESET = '' if content.endswith( RESET ) else RESET,
    )


_styles = dict(
    red = 31,
    green = 32,
    yellow = 33,
    blue = 34,
    magenta = 35,
    cyan = 36,
    white = 37,
    bold = 1,
    underline = 4,
)


filter_red = lambda c: wrap_style( c, _styles['red'] )
"""
make the input red

Examples::

    error: {{message|red}}
"""

filter_green = lambda c: wrap_style( c, _styles['green'] )
"""make the input green"""

filter_yellow = lambda c: wrap_style( c, _styles['yellow'] )
"""make the input yellow"""

filter_blue = lambda c: wrap_style( c, _styles['blue'] )
"""make the input blue"""

filter_magenta = lambda c: wrap_style( c, _styles['magenta'] )
"""make the input magenta"""

filter_cyan = lambda c: wrap_style( c, _styles['cyan'] )
"""make the input cyan"""

filter_white = lambda c: wrap_style( c, _styles['white'] )
"""make the input white"""

filter_bold = lambda c: wrap_style( c, _styles['bold'] )
"""
bold the input; may be combined with a color or other decoration

Examples::

    {{task.ticket}}: {{task.duration | durationfmt | bold | magenta}}
"""

filter_underline = lambda c: wrap_style( c, _styles['underline'] )
"""underline the input; may be combined with a color or other decoration"""


def filter_style( c, *styles ):
    """
    stylize the input with one or more decorations defined by arguments

    Args:
        styles (\*args): one or mor strings naming styles to apply, ``None`` to not
            apply any style

    This is really just another way to use the color and bold filters. It's useful
    if you want to pass the style names as strings instead of call their filters.

    Examples::

        error: {{message|style('red')}}

        {{task.ticket | style('bold' if task.isuploaded else None)}}
    """
    for style in styles:
        if style is None:
            continue
        c = wrap_style( c, _styles[style] )

    return c



def environment():
    env = Environment(
        trim_blocks = True,
        lstrip_blocks = True,
        extensions = [ 'jinja2.ext.loopcontrols' ],
        loader = ChoiceLoader( [
            FileSystemLoader( str( Config.data_home / 'views' ) ),
            PackageLoader( 'dagr', 'views' ),
        ] )
    )

    for name, function in inspect.getmembers( sys.modules[__name__], inspect.isfunction ):
        if name.startswith( 'filter_' ):
            name = name[7:]
            env.filters[name] = function

    return env



def render( template_name, args, dagr_view = None ):
    """
    Render and return the output of a template.

    Args:
        template_name (str): the name of the template to render
        args (dagr.controllers.base.BaseArguments): passed to the template as ``args``
        dagr (list of TaskView): If provided, defines the set of TaskView objects to
            pass to the template as ``dagr``. If omitted, ``dagr = None`` will be
            passed to the template.

    Returns:
        str: the rendered result from the template
    """
    template = environment().get_template( template_name )
    return template.render( args = args, dagr = dagr_view ).rstrip()



def render_fragment( fragment, **context ):
    """
    Render and return the output of a template fragment.

    Args:
        fragment (str): the template content
        context (kwargs): the variables passed to the template fragment

    Returns:
        str: the rendered result from the template fragment
    """
    template = environment().from_string( fragment )
    return template.render( **context ).rstrip()

