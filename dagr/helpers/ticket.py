
import re



LOOKS_LIKE_TICKET = re.compile( r'^[a-z]+-[0-9]+$', re.IGNORECASE )



def looks_like_ticket( candidate ):
    """
    Determine if a string is structured like a ticket number.

    A ticket number looks like ``"ABC-123"``, case insensitive.

    Args:
        candidate (str): the string under test

    Returns:
        bool: ``True`` if ``candidate`` is likely a ticket number, ``False`` otherwise
    """
    return LOOKS_LIKE_TICKET.match( candidate ) is not None

