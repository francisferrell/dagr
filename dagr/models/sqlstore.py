"""
methods to read and write dagr models using sqlite3
"""

from datetime import datetime, date
from pathlib import Path
import sqlite3

from pytz import utc

from ..helpers.config import Config
from ..helpers import tz

from .task import Task, End
from .dagr import Dagr



def _datetime_to_sql( value ):
    return int( value.astimezone( utc ).timestamp() )



def _datetime_from_sql( value ):
    return tz.local_fromtimestamp( int( value, base = 10 ) )



class SqlStore:
    """
    ``SqlStore`` provides access to an sqlite3 backend for storing :mod:`dagr.models` objects.

    Args:
        path (str or pathlib.Path): the path to the database file. If omitted, it will default to
            ``$XDG_DATA_HOME/dagr/data.sqlite3``.  Provide ``':memory:'`` to connect to an
            in-memory database.

    Raises:
        TypeError: if ``path`` is not a supported type
        IOError: if ``path`` is not a supported type
    """

    def __init__( self, path = None ):
        if path is None:
            path = Config.data_home / 'data.sqlite3'
        elif path == ':memory:':
            pass
        elif isinstance( path, str ):
            path = Path( path )
        elif isinstance( path, Path ):
            pass
        else:
            raise TypeError( 'unexecpted path: {!r}'.format( path ) )

        if isinstance( path, Path ) and not path.parent.exists():
            path.parent.mkdir( parents = True )

        sqlite3.register_adapter( datetime, _datetime_to_sql )
        sqlite3.register_converter( 'TIMESTAMP', _datetime_from_sql )

        sqlite3.register_adapter( bool, int )
        sqlite3.register_converter( "BOOLEAN", lambda v: bool( int( v ) ) )

        self.db = sqlite3.connect(
            str( path ),
            isolation_level = 'IMMEDIATE',
            detect_types = sqlite3.PARSE_DECLTYPES,
        )

        self.db.execute(
            '''
            CREATE TABLE IF NOT EXISTS tasks (
                klass TEXT NOT NULL,
                start TIMESTAMP PRIMARY KEY,
                description TEXT NOT NULL,
                ticket TEXT,
                uploaded BOOLEAN
            )
            '''
        )


    def load( self, when ):
        """
        Load and return the dagr records that start on the specified day, ``when``.

        Args:
            when (datetime.date): the date of the desired Tasks.

        Returns:
            :class:`dagr.models.Dagr`

        Raises:
            TypeError: if ``when`` is not a recognized type
            IOError: if there is a problem opening the file
        """
        if not isinstance( when, date ):
            raise TypeError( 'expected datetime.date: {}'.format( type( when ).__name__ ), when )

        with self.db:
            records = self.db.execute(
                '''
                SELECT
                    klass,
                    start,
                    description,
                    ticket,
                    uploaded
                FROM tasks
                WHERE   start >= :morning
                    AND start <= :night
                ORDER BY start ASC
                ''',
                dict(
                    morning = tz.local_datetime( when.year, when.month, when.day, hour = 0, minute = 0, second = 0 ),
                    night = tz.local_datetime( when.year, when.month, when.day, hour = 23, minute = 59, second = 59 ),
                )
            )

        return Dagr( _record_to_task( r ) for r in records.fetchall() )


    def save( self, dagr ):
        """
        Store the records in ``dagr``.

        Args:
            dagr (:mod:`dagr.models.Dagr`): the dagr records to save

        Returns:
            None

        Raises:
            ValueError: if ``dagr`` is empty
        """
        if len( dagr ) + len( dagr.trash ) == 0:
            raise ValueError( 'dagr must not be empty' )

        with self.db:
            self.db.executemany(
                '''
                DELETE FROM tasks WHERE start = :start
                ''',
                ( dict( start = t.start ) for t in dagr.trash )
            )

            self.db.executemany(
                '''
                INSERT OR REPLACE INTO tasks (klass, start, description, ticket, uploaded)
                VALUES (:klass, :start, :description, :ticket, :uploaded)
                ''',
                ( _task_to_record( t ) for t in dagr )
            )


    def ticket_is_known( self, ticket ):
        """
        Check if the specified ticket is known to exist.

        If any task in the database has the specified ticket, ``True`` is returned;
        otherwise ``False`` is returned. The match is case insensitive.

        Args:
            ticket (str): the ticket in question

        Returns:
            bool
        """
        with self.db:
            records = self.db.execute(
                '''
                SELECT EXISTS (SELECT 1 FROM tasks WHERE LOWER(ticket) = :ticket)
                ''',
                dict( ticket = ticket.lower() )
            )

        return bool( records.fetchone()[0] )



def _task_to_record( task ):
    """convert a Task into a dict of values for an INSERT statement"""
    return dict(
        klass = type( task ).__name__,
        start = task.start,
        description = task.description,
        ticket = task.ticket,
        uploaded = task.uploaded,
    )



def _record_to_task( record ):
    """convert a record from a SELECT statement to a Task"""
    konstructor = globals()[record[0]]
    return konstructor(
        start = record[1],
        description = record[2],
        ticket = record[3],
        uploaded = record[4],
    )

