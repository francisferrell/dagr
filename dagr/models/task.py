
from datetime import datetime



class Task:
    """
    ``Task`` represents the simple concept of starting on a certain description of work at a
    certain time.

    ``Task`` instances can be compared using the built in operators (``==``, ``<``, ``>=``, etc.)
    and so can be sorted in a list without using ``key``. Sort order will be delegated to the
    ``start`` property.

    A ``Task`` instance can also be compared with a ``datetime.datetime``; though, be aware of the
    truncation of ``start`` -- a ``Task`` is not guaranteed to be ``==`` to the original value
    assigned to its ``start`` property.

    Args:
        start (datetime): the start time of the task
        description (str): the description of work
        ticket (str): the ticket number; optional
        uploaded (bool): whether or not the work has been uploaded to jira

    Attributes:
        description (str): the description of work
        ticket (str or None): the ticket number
        uploaded (bool): whether or not the work has been uploaded to jira
    """

    def __init__( self, start, description, ticket = None, uploaded = False ):
        self.start = start
        self.description = description.strip()
        self.ticket = ticket
        self.uploaded = uploaded


    @property
    def start( self ):
        """
        datetime: the start time of the task

        Note:
            ``start`` is always truncated to the minute. No rounding is performed, ``second`` and
            ``microsecond`` are simply set to ``0``.
        """
        return self.__dict__['start']


    @start.setter
    def start( self, other ):
        self.__dict__['start'] = datetime(
            year = other.year,
            month = other.month,
            day = other.day,
            hour = other.hour,
            minute = other.minute,
            second = 0,
            microsecond = 0,
            tzinfo = other.tzinfo
        )


    def __repr__( self ):
        return '<{} start={!r} ticket={!r} description={!r} uploaded={!r}>'.format(
            type( self ).__name__,
            self.start,
            self.ticket,
            self.description,
            self.uploaded,
        )


    def __lt__( self, other ):
        if isinstance( other, datetime ):
            return self.start < other
        else:
            return self.start < other.start

    def __le__( self, other ):
        if isinstance( other, datetime ):
            return self.start <= other
        else:
            return self.start <= other.start

    def __eq__( self, other ):
        if isinstance( other, datetime ):
            return self.start == other
        else:
            return self.start == other.start

    def __ne__( self, other ):
        if isinstance( other, datetime ):
            return self.start != other
        else:
            return self.start != other.start

    def __gt__( self, other ):
        if isinstance( other, datetime ):
            return self.start > other
        else:
            return self.start > other.start

    def __ge__( self, other ):
        if isinstance( other, datetime ):
            return self.start >= other
        else:
            return self.start >= other.start



class End( Task ):
    """
    ``End`` represents the end of a continuous sequence of work tasks.

    Args:
        start (datetime): the end time of the previous task

    Attributes:
        start (datetime): the end time of the previous task (see Task for notes)
        description (str): always the empty string
        ticket (str): always None
        uploaded (bool): always False
    """

    def __init__( self, start, *_, **__ ):
        super().__init__( start = start, description = '', ticket = None, uploaded = False )


    def __repr__( self ):
        return '<{} start={!r}>'.format(
            type( self ).__name__,
            self.start
        )

