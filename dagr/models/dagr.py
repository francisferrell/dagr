
from collections.abc import MutableSequence
from datetime import datetime



class Dagr( MutableSequence ):
    """
    ``Dagr`` is a ``list``-like object that:

        * keeps itself sorted at instantiation and after every growth operation.
        * stores only unique members, as determined by ``==``.

    Index assignment, slice assignment, and ``reverse()`` raise ``NotImplementedError``.

    Items may be added using ``append()``, ``insert()``, or ``extend()``. Newly added items replace
    existing members which are equal. After adding to the ``Dagr``, the new element's index
    may not be what you expect, since the list will be re-sorted after the insert operation.

    Items can be removed with ``del``, ``pop()``, and ``remove()``.

    Items can be indexed either by integer indexes as usual, or by ``datetime.datetime``. When
    a datetime is used in index notation, its seconds and microseconds will be set to ``0`` and
    then the Task whose ``start`` is that datetime will be returned (or ``IndexError`` raised).

    If the ``Dagr`` contains objects that are comparable by one of their properties, changing
    that property on an element in the ``Dagr`` can violate the sorted and unique principles
    since the ``Dagr`` doesn't have an opportunity to re-sort itself after you mutate one of its
    member's properties.

    No, this is not a high performance container. In terms of what dagr needs to do, the
    datasets aren't even minor, they are trivial.
    """

    def __init__( self, iterable = None ):
        self._data = list()
        self._trash = list()
        if iterable is not None:
            self.extend( iterable )


    def __repr__( self ):
        return '<{} {!r}>'.format( type( self ).__name__, self._data )


    def __delitem__( self, i ):
        if isinstance( i, datetime ):
            i = i.replace( second = 0, microsecond = 0 )
            try:
                i, deleted = next( ( idx, task ) for idx, task in enumerate( self._data ) if task.start == i )
            except StopIteration:
                raise IndexError( 'list index out of range' )
        else:
            deleted = self._data[i]

        self._trash.append( deleted )
        del self._data[i]


    @property
    def trash( self ):
        """
        list: contains the items that have been removed from the Dagr since it was instantiated.
        """
        return self._trash


    def __getitem__( self, i ):
        if isinstance( i, slice ):
            return Dagr( self._data[i] )
        elif isinstance( i, datetime ):
            i = i.replace( second = 0, microsecond = 0 )
            try:
                return next( t for t in self._data if t.start == i )
            except StopIteration:
                raise IndexError( 'list index out of range' )
        else:
            return self._data[i]


    def __len__( self ):
        return len( self._data )


    def __setitem__( self, i, v ):
        raise NotImplementedError


    def reverse( self ):
        """
        Raises:
            NotImplementedError
        """
        raise NotImplementedError


    def insert( self, i, v ):
        """
        Inserts ``v`` into the Dagr

        Notes:
            This method *completely ignores* the insertion position, ``i``, since the list has to be
            sorted after the insertion operation anyways.

            You shouldn't attempt to insert an element into a specific position in the
            ``Dagr``, since that might not be the correctly sorted position. This method is
            here because it is required to subclass ``collections.abc.MutableSequence`` and for API
            ompatibility with ``list``.
        """
        if v in self._data:
            self._data = [ x for x in self._data if x != v ]
        self._data.append( v )
        self._data.sort()

