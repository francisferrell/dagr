"""
The fundamental data structures of dagr.

These data models make a few important assumptions:

    * ``Task`` objects are only useful in a sequence representing a continuous stream of work.
      Therefore, a Task object only has a ``start``. It is the responsibility of the surrounding
      data context to provide an ``end``, and thus the ``duration``. ``Dagr`` provides this.
    * a ``Task`` object with a duration less than a minute is not interesting. Therefore, Task
      objects truncate their ``start`` to the minute.
    * ``Dagr``  assumes that the calling code wants the stream to remain sorted chronologically
      at all times and that the list of Task objects will change only by addition to and deletion
      from the list. Individual Task objects in the list will never be directly assigned (using
      index assignment),  that the member Task objects' start attribute will never be mutated after
      the Task is inserted.
"""

from .task import Task, End
from .dagr import Dagr
from .sqlstore import SqlStore

__all__ = [ 'Dagr', 'Task', 'End', 'SqlStore' ]

