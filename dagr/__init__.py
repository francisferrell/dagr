"""
``dagr`` is structured using MVC. Fundamental data structures are in :mod:`.models`. CLI
entry point is in :mod:`.controllers`. View templates are in ``views/``, which contains no
python code, only jinja2 templates.
"""



__title__ = 'dagr'
__version__ = 'git'
__description__ = 'a simple tool for tracking time spent on tasks'
__author__ = 'Francis Ferrell'
__author_email__ = 'francisferrell@gmail.com'
__copyright__ = 'Copyright 2020 {}'.format( __author__ )
__url__ = 'https://francisferrell.gitlab.io/dagr/'
__license__ = 'MIT'



class DagrError( Exception ):
    """
    Base error class
    """

    def __init__( self, fmt, *args, **kwargs ):
        super().__init__( fmt.format( *args, **kwargs ) )

