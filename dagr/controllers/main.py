
import sys

from .. import DagrError, __version__
from ..helpers.config import ConfigError
from ..helpers.view import render, TemplateNotFound

from . import start, end, delete, upload, report
from .base import BaseArguments, ArgumentError



def main():
    """
    the main entry point for ``dagr``
    """
    try:
        args = BaseArguments.parse()

        if args.empty:
            print( render( 'help/usage.j2', args ) )
            return 0

        if args.help:
            try:
                print( render( 'help/{}.j2'.format( 'main' if args.command is None else args.command ), args ) )
                return 0
            except TemplateNotFound:
                raise ArgumentError( 'unknown command: {args.command}', args = args )

        if args.version:
            print( __version__ )
            return 0

        args.validate()

        if args.command == 'start':
            start.main( args )
        elif args.command == 'end':
            end.main( args )
        elif args.command == 'delete':
            delete.main( args )
        elif args.command == 'upload':
            upload.main( args )
        else:
            report.main( args )

    except ArgumentError as err:
        sys.stderr.write( '{args.program}: {err}\n'.format( args = args, err = err ) )
        return 1
    except ConfigError as err:
        sys.stderr.write( '{args.program}: {err.config.path}: {err}\n'.format( args = args, err = err ) )
        return 1
    except DagrError as err:
        sys.stderr.write( '{args.program}: {err}\n'.format( args = args, err = err ) )
        return 1
    else:
        return 0

