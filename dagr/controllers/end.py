"""
``dagr [DATEREF] end [TIMEREF]``
"""

from .. import libdagr
from .base import BaseArguments, ArgumentMixinTimeref



class Arguments( BaseArguments, ArgumentMixinTimeref ):
    """
    The parsed and validated form of the command line arguments passed to ``dagr end``.

    Attributes:
        timeref (str or datetime.datetime): string after parsing but before validation,
            ``datetime.datetime`` after validation. Defaults to the current time. Both the default
            and the command line provided valure are interpreted relative to now on ``dateref``.
    """

    def __init__( self, *args ):
        super().__init__( *args )
        self.__init__timeref__()


    def validate( self ):
        super().validate()
        self.validate_timeref()



def main( args ):
    """
    Create the End represented by ``args``, appending it to ``dagr`` and saving using
    ``backend``.
    """
    libdagr.end( datetime = args.timeref )

