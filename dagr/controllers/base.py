
from os.path import basename
import sys

from .. import DagrError
from ..helpers.dateref import looks_like_dateref, dateref_to_date, InvalidDateref
from ..helpers.timeref import looks_like_timeref, timeref_to_datetime, InvalidTimeref



class ArgumentError( DagrError ):
    """
    Any parsing/validation error of a command line argument passed to ``dagr``
    """



class BaseArguments:
    """
    The parsed and validated form of the command line arguments passed to ``dagr``.

    This class includes all args common to every command. Most commands have a subclass that adds
    additional args.

    The caller should use :func:`BaseArguments.parse` instead of instantiating the Arguments
    directly, to ensure the caller receives the correct subclass.

    Attributes:
        program (str): ``basename( argv[0] )``, generally ``"dagr"``
        empty (bool): ``True`` if ``argv`` was empty beyond ``program``, ``False`` if more args were
            encountered.
        help (bool): whether or not help was requested via ``-h`` or ``--help``
        dateref (str or datetime.date): string after parsing but before validation, ``datetime.date``
            after validation. Defaults to today.
        command (str or None): the command name, ``"start"``, ``"report"``, etc. Defaults to ``None``.
    """


    @staticmethod
    def parse( argv = None ):
        """
        parse arguments into a ``BaseArguments`` instance, or one of its subclasses as appropriate.

        Args:
            argv (list of str): the arguments to parse; defaults to ``sys.argv``. if you pass your
                own, note that argv[0] must be the program name, e.g. ``"dagr"``, not the first
                argument value.

        Returns
            BaseArguments: the successfully parsed arguments; may be a subclass
        """
        if argv is None:
            argv = sys.argv

        program = basename( argv[0] )
        empty = len( argv ) == 1
        help = False
        version = False
        dateref = '@today'
        command = None
        argv = argv[1:]

        try:
            if '-h' in argv or '--help' in argv:
                help = True
                argv = [ x for x in argv if x not in { '-h', '--help' } ]

            if '--version' in argv:
                version = True
                argv = [ x for x in argv if x != '--version' ]

            if looks_like_dateref( argv[0] ):
                dateref = argv.pop(0)

            command = argv.pop(0)
        except IndexError:
            pass

        # whenever you do something clever. you'll regret it later
        # a long time from when you do the clever thing, you're going to have forgotten how it was
        # clever. then when you come back and look at it, it's going to be confusing because
        # you don't know how it works.
        #
        # shame on you, past Francis.
        subclasses = { cls.__module__.split( '.' )[-1]: cls for cls in BaseArguments.__subclasses__() }
        if command is None or command not in subclasses:
            klass = BaseArguments
        else:
            klass = subclasses[command]

        return klass( program, empty, help, version, dateref, command, argv )


    def __init__( self, program, empty, help, version, dateref, command, argv ):
        self.program = program
        self.empty = empty
        self.help = help
        self.version = version
        self.dateref = dateref
        self.command = command
        self.argv = argv


    def __repr__(self):
        return '{}({})'.format(
            type( self ).__name__,
            ', '.join( '{}={!r}'.format( *pair ) for pair in sorted( self.__dict__.items() ) )
        )


    def validate( self ):
        """
        Validate all known arguments.

        Raises:
            ArgumentError: if an argument is invalid
        """
        if self.dateref is not None:
            try:
                self.dateref = dateref_to_date( self.dateref )
            except InvalidDateref as err:
                raise ArgumentError( str( err ) )

        if self.command is None:
            raise ArgumentError( 'command is required' )

        if len( self.argv ) > 0:
            raise ArgumentError( 'too many arguments: {!r}', self.argv )



class ArgumentMixinTimeref:
    """
    This class exists so that commands that accept a TIMEREF can share argument parsing logic.
    """

    def __init__timeref__( self ):
        self.timeref = '@now'

        try:
            if looks_like_timeref( self.argv[0] ):
                self.timeref = self.argv.pop(0)
        except IndexError:
            pass


    def validate_timeref( self ):
        if self.timeref is not None:
            try:
                self.timeref = timeref_to_datetime( self.timeref, relative_to = self.dateref )
            except InvalidTimeref as err:
                raise ArgumentError( str( err ) )

        if self.dateref != self.timeref.date():
            raise ArgumentError( 'dateref and timeref must be on the same day: dateref={:%Y-%m-%d} timeref={:%Y-%m-%d %H:%M}', self.dateref, self.timeref )

