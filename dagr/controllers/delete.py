"""
``dagr [DATEREF] delete TIMEREF``
"""

from .. import libdagr
from .base import BaseArguments, ArgumentError, ArgumentMixinTimeref



class Arguments( BaseArguments, ArgumentMixinTimeref ):
    """
    The parsed and validated form of the command line arguments passed to ``dagr delete``.

    Attributes:
        timeref (str or datetime.datetime): string after parsing but before validation,
            ``datetime.datetime`` after validation. Defaults to the current time. Both the default
            and the command line provided valure are interpreted relative to now on ``dateref``.
    """

    def __init__( self, *args ):
        super().__init__( *args )
        self.__init__timeref__()


    def validate( self ):
        super().validate()
        self.validate_timeref()



def main( args ):
    """
    Remove the entry starting at ``args.timeref``, if it exists.
    """
    try:
        libdagr.delete( datetime = args.timeref )
    except IndexError:
        raise ArgumentError( 'no task found starting at {timeref:%Y-%m-%d %H:%M}', timeref = args.timeref )

