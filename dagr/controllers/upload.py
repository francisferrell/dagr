"""
``dagr [DATEREF] upload [-f|--force]``
"""

import sys

from .. import libdagr, DagrError
from .base import BaseArguments
from ..helpers.view import render_fragment



class Arguments( BaseArguments ):
    """
    The parsed and validated form of the command line arguments passed to ``dagr upload``.
    """

    def __init__( self, *args ):
        super().__init__( *args )
        self.force = False

        try:
            if self.argv[0] in { '-f', '--force' }:
                self.argv.pop(0)
                self.force = True
        except IndexError:
            pass



def main( args ):
    """
    End and upload to jira the completed ``dagr``.
    """
    def before_upload( task_view ):
        try:
            sys.stdout.write( render_fragment(
                'uploading {{task_view.duration | durationfmt}} to {{task_view.ticket}}... ',
                task_view = task_view
            ) )
            sys.stdout.flush()
        except Exception as err:
            sys.stdout.write( '{}\n'.format( err ) )

    def after_upload( task_view, err ):
        if err is None:
            sys.stdout.write( 'OK\n' )
        else:
            sys.stdout.write( '{}\n'.format( err ) )

    uploaded = libdagr.upload(
        date = args.dateref,
        force = args.force,
        before = before_upload,
        after = after_upload
    )

    if not uploaded:
        raise DagrError( 'nothing to upload.' )

