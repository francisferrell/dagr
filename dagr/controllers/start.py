"""
``dagr [DATEREF] start [TIMEREF] [TICKET] DESCRIPTION [...]``
"""

from .. import libdagr
from .base import BaseArguments, ArgumentError, ArgumentMixinTimeref



class Arguments( BaseArguments, ArgumentMixinTimeref ):
    """
    The parsed and validated form of the command line arguments passed to ``dagr start``.

    Attributes:
        timeref (str or datetime.datetime): string after parsing but before validation,
            ``datetime.datetime`` after validation. Defaults to the current time. Both the default
            and the command line provided valure are interpreted relative to now on ``dateref``.
        ticket (str or None): the ticket, if any
        description (str): the description if any, otherwise ``""``
    """


    def __init__( self, *args ):
        super().__init__( *args )
        self.__init__timeref__()

        self.description = ''
        self.ticket = None

        try:
            if libdagr.looks_like_ticket( self.argv[0] ):
                self.ticket = self.argv.pop(0)

            self.description = ' '.join( self.argv )
            self.argv = []
        except IndexError:
            pass


    def validate( self ):
        super().validate()
        self.validate_timeref()

        if self.description == '' and self.ticket is None:
            raise ArgumentError( 'description or ticket is required' )



def main( args ):
    """
    Create the Task represented by ``args``, appending it to ``dagr`` and saving using
    ``backend``.

    If ``ticket`` is provided and jira integration is included in ``config``, verify that the ticket
    exists. Furthermore, if ``description`` is not provided, fill it in with the ticket summary from
    jira.
    """
    try:
        libdagr.start(
            datetime = args.timeref,
            ticket = args.ticket,
            description = args.description,
        )
    except libdagr.IssueNotFound as err:
        raise ArgumentError( str( err ) )

