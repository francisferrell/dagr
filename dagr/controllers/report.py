"""
``dagr [DATEREF] report``
"""

from .. import libdagr
from .base import ArgumentError
from ..helpers.view import render, TemplateNotFound



def main( args ):
    """
    Render and write to stdout the content of the template named ``args.command``.
    """
    dagr = libdagr.get_tasks( date = args.dateref )

    try:
        print( render( '{}.j2'.format( args.command ), args, dagr ) )
    except TemplateNotFound:
        raise ArgumentError( 'unknown command: {args.command}', args = args )

