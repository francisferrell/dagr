"""
An externally facing library for other applications to integrate with
and provide a frontend to dagr.

.. data:: version

    *str* -- the version of the dagr package

.. autoexception:: DagrError

.. autoclass:: Config
.. autoexception:: ConfigError

.. autofunction:: dateref_to_date
.. autoexception:: InvalidDateref
.. autofunction:: looks_like_dateref

.. autofunction:: timeref_to_datetime
.. autoexception:: InvalidTimeref
.. autofunction:: looks_like_timeref

.. autofunction:: looks_like_ticket

.. autoexception:: IssueNotFound
.. autoexception:: JIRAError
"""

from .helpers.config import Config, ConfigError
from .helpers.dateref import dateref_to_date, InvalidDateref, looks_like_dateref
from .helpers.ticket import looks_like_ticket
from .helpers.timeref import timeref_to_datetime, InvalidTimeref, looks_like_timeref
from .helpers import tz
from .helpers.jira import Jira, IssueNotFound, JIRAError
from .models import SqlStore, Task, End
from . import DagrError
from . import __version__ as version



class FakeEnd( End ):
    """
    A subclass of :mod:`dagr.models.End` for use when a Dagr ends with a Task instead of a End.

    Attributes:
        start (datetime): ``datetime.now()``
        description (str): always the empty string
    """

    def __init__( self, *_, **__ ):
        super().__init__( start = tz.local_now() )



class TaskView():
    """
    A transformation of ``models.Task`` providing some extra, computed attributes that are not
    present on the lower level model. As with the ``Task`` model, all times are truncated to
    the minute.

    In the view template, ``dagr`` will be an array of these.

    Attributes:
        task (:class:`dagr.models.Task`): the underlying ``Task`` model
        start (datetime): the date and time the work started
        end (datetime): the date and time the work ended
        duration (timedelta): ``end - start``
        ticket (str): an associated ticket, may be the empty string
        description (str): the description of work, may be the empty string
        isopen (bool): ``True`` if this task is still ongoing, i.e. ``end`` hasn't actually been
            defined as an end of the work or the start of a new task. When ``True``, ``end`` will
            be ``datetime.now()``.
        isterminal (bool): ``True`` if this task is explicitly closed, ``False`` if it is still open
            or is closed implicitly by another task starting. Stated another way, this attribute
            indicates whether or not this task is the last one of its contiguous sequence. For tasks
            where this is ``True``, deleting their end time will reopen the task. For tasks where
            this is ``False``, deleting their end time will actually delete the next task, which is
            probably not the user's intention.
        isuploaded (bool): ``True`` if this task has been uploaded to jira already.
    """

    def __init__( self, task, start, end, ticket, description, isopen, isterminal, isuploaded ):
        self.task = task
        self.start = start
        self.end = end
        self.duration = self.end - self.start
        self.ticket = ticket if ticket else ''
        self.description = description
        self.isopen = isopen
        self.isterminal = isterminal
        self.isuploaded = isuploaded


    def __repr__( self ):
        return '{}({})'.format(
            type( self ).__name__,
            ', '.join( '{}={!r}'.format( *pair ) for pair in sorted( self.__dict__.items() ) )
        )



def _resolve_date( dateref = None, date = None ):
    if dateref is not None and date is not None:
        raise ValueError( "expected either dateref or date but received both" )

    if date is not None:
        return date

    if dateref is not None:
        return dateref_to_date( dateref )

    return dateref_to_date( '@today' )



def _resolve_date_and_time( dateref = None, timeref = None, datetime = None ):
    if ( dateref is not None or timeref is not None ) and datetime is not None:
        raise ValueError( "expected either dateref+timeref or datetime but received both" )

    if dateref is not None and timeref is None:
        raise ValueError( "received dateref without timeref" )

    if datetime is not None:
        if datetime.tzinfo is not None:
            return datetime
        else:
            return tz.localize( datetime )

    if dateref is not None:
        date = _resolve_date( dateref = dateref )
        return timeref_to_datetime( timeref, relative_to = date )

    return timeref_to_datetime( '@now' )



def _dagr_to_task_views( dagr ):
    dagr = list( dagr )

    if len( dagr ) > 0 and not isinstance( dagr[-1], End ):
        dagr.append( FakeEnd() )

    dagr_view = list()

    for lhs, rhs in zip( dagr[:-1], dagr[1:] ):
        if isinstance( lhs, End ):
            continue

        dagr_view.append( TaskView(
            task = lhs,
            start = lhs.start,
            end = rhs.start,
            ticket = lhs.ticket,
            description = lhs.description,
            isopen = isinstance( rhs, FakeEnd ),
            # can't use isinstance here because FakeEnd is a subclass of End
            isterminal = type( rhs ) is End,
            isuploaded = lhs.uploaded,
        ) )

    return dagr_view



def get_tasks( dateref = None, date = None ):
    """
    Get a list of Tasks for the specified day.

    You may either provide a ``dateref`` or a ``date``, whichever is more convenient.
    If neither are provided, "today" is assumed. It is an error to provide both.

    Args:
        dateref (str, optional): the specified day
        date (datetime.date, optional): the specified day

    Returns:
        list of :class:`dagr.helpers.view.TaskView`

    Raises:
        InvalidDateref: if ``dateref`` is provided and is not a valid dateref
        ValueError: if both ``date`` and ``dateref`` are provided
    """
    date = _resolve_date( dateref = dateref, date = date )
    backend = SqlStore()
    dagr = backend.load( date )
    return _dagr_to_task_views( dagr )



def start( dateref = None, timeref = None, datetime = None, ticket = None, description = None ):
    """
    Start new work.

    If another entry exists starting at the same time, it will be replaced with this one.

    You may either provide a ``dateref`` or a ``date``, whichever is more convenient.
    If neither are provided, "today" is assumed. It is an error to provide both.

    You may either provide a ``dateref`` + ``timeref`` or a ``datetime``, whichever is more
    convenient. It is an error to provide either ``dateref`` or ``timeref`` when providing
    ``datetime``. It is an error to provide ``dateref`` and omit ``timeref``. If only ``timeref``
    is provided, the default date is "today". If none are provided, the default datetime is "now".

    If Jira integration is configured and ``ticket`` is provided, then ``ticket`` will be verified
    to exist. Additionally, if ``description`` is omitted or blank, it will default to the summary
    of the ``ticket``.

    Args:
        dateref (str, optional): a dateref
        timeref (str, optional): a timeref
        datetime (datetime.datetime, optional): a specific date and time
        ticket (str, optional): the ticket number
        description (str): the description of work; optional if ``ticket`` is provided

    Returns:
        ``True``

    Raises:
        InvalidDateref: if ``dateref`` is provided and is not a valid dateref
        InvalidTimeref: if ``timeref`` is provided and is not a valid timeref
        ValueError: if both ``datetime`` and ``dateref`` are provided
        ValueError: if both ``datetime`` and ``timeref`` are provided
        ValueError: if neither ``ticket`` nor ``description`` are provided
        ConfigError: if there is an error in the user's dagr configuration file
        IssueNotFound: if ``ticket`` is provided, but not found in the configured Jira backend
        JIRAError: if Jira integration is configured and an error (other than IssueNotFound)
            occurs
    """
    if description is None:
        description = ''

    if ticket is None and description == '':
        raise ValueError( 'expected ticket or description, but received neither' )

    start_time = _resolve_date_and_time( dateref = dateref, timeref = timeref, datetime = datetime )
    config = Config()
    backend = SqlStore()
    dagr = backend.load( start_time.date() )

    task = Task( start_time, description, ticket = ticket )

    if task.ticket is not None and config.jira is not None:
        if backend.ticket_is_known( task.ticket ) and task.description != '':
            # we've seen this ticket before and the description doesn't need to be auto-filled
            pass
        else:
            # either the ticket's existance needs to be verified
            # or the description needs to be auto-filled
            jira = Jira( config )
            issue = jira.get_issue( task.ticket, fields = 'summary' )
            if task.description == '':
                task.description = issue.fields.summary

    dagr.append( task )
    backend.save( dagr )
    return True



def end( dateref = None, timeref = None, datetime = None ):
    """
    End work.

    If another entry exists starting at the same time, it will be deleted.

    You may either provide a ``dateref`` + ``timeref`` or a ``datetime``, whichever is more
    convenient. It is an error to provide either ``dateref`` or ``timeref`` when providing
    ``datetime``. It is an error to provide ``dateref`` and omit ``timeref``. If only ``timeref``
    is provided, the default date is "today". If none are provided, the default datetime is "now".

    Args:
        dateref (str, optional): a dateref
        timeref (str, optional): a timeref
        datetime (datetime.datetime, optional): a specific date and time

    Returns:
        ``True``

    Raises:
        InvalidDateref: if ``dateref`` is provided and is not a valid dateref
        InvalidTimeref: if ``timeref`` is provided and is not a valid timeref
        ValueError: if both ``datetime`` and ``dateref`` are provided
        ValueError: if both ``datetime`` and ``timeref`` are provided
    """
    end_time = _resolve_date_and_time( dateref = dateref, timeref = timeref, datetime = datetime )

    backend = SqlStore()
    dagr = backend.load( end_time.date() )

    dagr.append( End( end_time ) )
    backend.save( dagr )
    return True



def delete( dateref = None, timeref = None, datetime = None ):
    """
    Remove an entry from the dagr, or reopen a dagr closed by ``end()``.

    If the specified time corresponds to the start time of work, removes that
    ticket/description from the dagr. If it corresponds to the end of a dagr,
    reopens that dagr.

    You may either provide a ``dateref`` + ``timeref`` or a ``datetime``, whichever is more
    convenient. It is an error to provide either ``dateref`` or ``timeref`` when providing
    ``datetime``. It is an error to provide ``dateref`` and omit ``timeref``. If only ``timeref``
    is provided, the default date is "today". If none are provided, the default datetime is "now".

    Args:
        dateref (str, optional): a dateref
        timeref (str, optional): a timeref
        datetime (datetime.datetime, optional): a specific date and time

    Returns:
        ``True``

    Raises:
        InvalidDateref: if ``dateref`` is provided and is not a valid dateref
        InvalidTimeref: if ``timeref`` is provided and is not a valid timeref
        ValueError: if both ``datetime`` and ``dateref`` are provided
        ValueError: if both ``datetime`` and ``timeref`` are provided
        IndexError: if the specified time does not exist in the specified dagr
    """
    datetime = _resolve_date_and_time( dateref = dateref, timeref = timeref, datetime = datetime )

    backend = SqlStore()
    dagr = backend.load( datetime.date() )

    del dagr[datetime]
    backend.save( dagr )
    return True



def upload( dateref = None, date = None, force = False, before = None, after = None ):
    """
    End work now (if ``end()`` has not yet been called) and upload the completed dagr to Jira.

    You may either provide a ``dateref`` or a ``date``, whichever is more convenient.
    If neither are provided, "today" is assumed. It is an error to provide both.

    All completed but not yet uploaded sequences within the referenced dagr will be uploaded.
    All completed and uploaded sequences will be ignored. Therefore, it is safe to call this
    method multiple times for the same day **as long as force=False** (the default).

    It is possible that no uploading will occur (all sequences for the specified dagr lack
    tickets or are already uploaded). Therefore, if you provide callback(s), do not rely on
    them ever being called. This method might take no action.

    Note that :meth:`start`, :meth:`end`, and :meth:`delete` do not prevent you from modifying an
    already-uploaded dagr, making it possible to create inconsistencies in the data. Use with
    caution, or your user can end up with duplicate, conflicting worklog entries in Jira.

    Args:
        dateref (str, optional): the specified day
        date (datetime.date, optional): the specified day
        force (bool, optional): if ``True``, upload the dagr, even it it was previously
            marked as successfully uploaded. the default is ``False``
        before (callable, optional): if provided, will be called right before each worklog
            request to Jira.
        after (callable, optional): if provided, will be called right after each worklog
            request to Jira.

    Returns:
        bool: ``True`` if there was something to upload, ``False`` if there was nothing

    Raises:
        InvalidDateref: if ``dateref`` is provided and is not a valid dateref
        ValueError: if both ``date`` and ``dateref`` are provided
        ConfigError: if there is an error in the user's dagr configuration file
        JIRAError: if an error occurs

    **Callbacks**

    The ``before`` and ``after`` callbacks can be provided to receive notification about the
    progress of the upload process. You may provide only one if you're not interested in both.

    *All exceptions raised by your callbacks are silently eaten! Including if your callback
    fails to accept the right signature*

    ``before`` Args:

        * **task** (:class:`TaskView`) -- the TaskView about to be uploaded.

    ``after`` Args:

        * **task** (:class:`TaskView`) -- the TaskView that was uploaded.

        * **error** (*Exception, optional*): ``None`` in the case of success; an Exception
          instance in the case of an error.
    """
    date = _resolve_date( dateref = dateref, date = date )

    config = Config()
    backend = SqlStore()
    dagr = backend.load( date )
    jira = Jira( config )

    if len( dagr ) == 0:
        return False

    if not isinstance( dagr[-1], End ):
        dagr.append( End( timeref_to_datetime( '@now', relative_to = date ) ) )

    dagr_view = _dagr_to_task_views( dagr )
    uploads = list()

    for task_view in dagr_view:
        if task_view.ticket is None or task_view.ticket == '':
            continue
        if task_view.isuploaded and not force:
            continue

        uploads.append( dict(
            task_view = task_view,
            ticket = task_view.ticket,
            start = task_view.start,
            duration = task_view.duration,
        ) )

    if len( uploads ) == 0:
        return False

    if not callable( before ):
        before = lambda *args: True

    if not callable( after ):
        after = lambda *args: True

    for upload in uploads:
        task_view = upload.pop( 'task_view' )

        try:
            before( task_view )
        except Exception as err:
            pass

        try:
            jira.upload_worklog( **upload )
        except Exception as err:
            try:
                after( task_view, err )
            except Exception as err:
                pass
        else:
            task_view.task.uploaded = True

            try:
                after( task_view, None )
            except Exception as err:
                pass

    backend.save( dagr )
    return True

