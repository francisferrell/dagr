
.DEFAULT_GOAL := build

DAGR_VERSION := $(shell awk '$$1 == "__version__" { print $$3 }' $(CURDIR)/dagr/__init__.py)


.PHONY: cleanpy
cleanpy:
	find . -type d -name __pycache__ -print0 | xargs -0 rm -rf
	rm -rf --one-file-system ./dagr.egg-info

.PHONY: clean
clean: cleanpy cleanvenv cleanapi
	rm -rf --one-file-system ./build



VENV := ./venv/build

venv/build/bin/activate:
	mkdir -p venv
	python3 -m venv venv/build
	./venv/build/bin/pip install -r requirements.build.txt
	./venv/build/bin/pip install -r requirements.txt
	./venv/build/bin/pip install -e .
.PHONY: venv
venv: venv/build/bin/activate

.PHONY: cleanvenv
cleanvenv:
	rm -rf --one-file-system ./venv


venv/dagr/bin/activate:
	mkdir -p venv
	python3 -m venv venv/dagr
	./venv/dagr/bin/pip install -r requirements.txt
	./venv/dagr/bin/pip install -e .
	@echo
.PHONY: develop
develop: venv/dagr/bin/activate
	@echo Now run:
	@echo '    . venv/dagr/bin/activate'

.PHONY: cleandevelop
cleandevelop:
	rm -rf --one-file-system ./venv/dagr


SUITE=tests/unit
.PHONY: unit
unit: venv
	$(VENV)/bin/python3 -m pytest -v $(SUITE) -p no:cacheprovider
.PHONY: test
test: unit


build/man/%: assets/man/%.rst venv
	mkdir -p build/man
	$(VENV)/bin/rst2man.py --verbose --strict assets/man/$(@F).rst $@

build/man/%.gz: build/man/%
	gzip --force $<


.PHONY: man
man: build/man/dagr.1.gz build/man/dagr.5.gz


.PHONY: apidocs
apidocs: venv
	rm -rf \
		./sphinx/_autosummary \
		./build/docs
	mkdir -p build/docs
	sh -c '. ${VENV}/bin/activate && make -C sphinx html'

.PHONY: cleanapi
cleanapi: cleanvenv
	rm -rf --one-file-system ./sphinx/_autosummary

.PHONY: serveapi
serveapi: apidocs
	sh -c 'cd build/docs/html && python3 -m http.server'


.PHONY: build
build: test apidocs man


.PHONY: buildversion
buildversion:
	@set -e ;\
		if [ -n "$${CI_COMMIT_TAG}" ] ; then \
			VERSION="$${CI_COMMIT_TAG#ubuntu/}" ;\
			echo "writing build version $${VERSION}" ;\
			sed -i -e "/^__version__[[:space:]]*=/ s%=.*%= '$${VERSION}'%" $(CURDIR)/dagr/__init__.py ;\
		else \
			echo "CI_COMMIT_TAG not set in environment, skipping build version" ;\
		fi



.PHONY: git_is_clean
git_is_clean:
	sh -c 'if [ -z "$$(git status --porcelain)" ]; then true; else echo "working copy is not clean"; false; fi'



.PHONY: release
release:
	@if [ -n "$(VERSION)" ]; then true; else echo "Error: VERSION not specified. If you're calling 'make release' directly, make sure you know what you're doing!"; exit 2; fi
	@if [ "$$( git rev-parse --abbrev-ref HEAD )" = 'main' ]; then true; else echo "Error: branch is '$$( git rev-parse --abbrev-ref HEAD )'; expected 'main'"; exit 2; fi
	@echo Releasing $(VERSION)!
	@echo
	git fetch origin
	git tag '$(VERSION)'
	git checkout ubuntu
	git reset --hard origin/ubuntu
	git merge main --no-edit --message 'merge main into ubuntu for release of $(VERSION)'
	gbp dch --new-version='$(VERSION)-1' --release --distribution=eoan --force-distribution --commit --commit-msg='release $(VERSION) for ubuntu' --spawn-editor=never
	git tag 'ubuntu/$(VERSION)-1'
	git checkout main
	@echo
	@echo Release workflow complete. Now run:
	@echo
	@echo '       git push origin ubuntu'
	@echo '       git push --tags'
	@echo



.PHONY: revbump minorbump majorbump
revbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1,$$2,$$3+1}' )"

minorbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1,$$2+1,0}' )"

majorbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1+1,0,0}' )"

